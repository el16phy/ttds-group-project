#! C:/Users/Lydia/Anaconda3/python.exe
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 20 14:14:28 2018

@author: Lydia
"""
import re
import math
import json
import pymysql
from nltk import word_tokenize
from nltk.corpus import stopwords
from Lemmatizer_method import Lemmatizer

class IR():
    
    def __init__(self):
        file = open('123','w')
        file.close()
        
        index = open('index.txt', 'r', encoding='utf-8')
        self.lines = index.readlines()
        index.close()
        
        self.conn = pymysql.connect("localhost", "root", "root", "ttds", charset='utf8' )
        self.c = self.conn.cursor()
        
        self.index_preprocess()
        self.query_preprocess()
    
    def index_preprocess(self):
      
        # to store the index into dictionary 
        # to count the number of documents in a collection
        allID = []
        terms = []
        docs_terms_pos = []
        docs_terms = []
        for line in self.lines[0:10000]:
            if re.match('\t', line) is None:
                docs_term_pos = {}
                docs_term = []
                term = line.split(":")[0]
                terms.append(term) 
                docs_term.append(term)
                print (term)
            if re.match('\t', line) is not None:
                docid = re.sub('\t', '', line.split(":")[0])
                allID.append(docid)
                docid = re.sub('\t', '', line.split(":")[0])
                docs_term_pos.setdefault(docid, [])
                docs_term.append(docid)
                term_pos = re.sub('\n', '', line.split(":")[1]).split(", ")
                docs_term_pos[docid]=term_pos 
                if docs_term_pos not in docs_terms_pos:
                    docs_terms_pos.append(docs_term_pos)
                if docs_term not in docs_terms:
                    docs_terms.append(docs_term)
        allID = set(allID)
        filename='aa'
        with open(filename+'.json','a') as outfile:
            json.dump(dict(zip(terms,docs_terms_pos)),outfile,ensure_ascii=False)
            outfile.write('\n')
        self.N = len(allID)
        self.index = dict(zip(terms,docs_terms_pos))
        self.docs_terms = docs_terms
           
    def query_preprocess(self):
        # read the newest query from database
        self.c.execute("SELECT query_id,original_query FROM query ORDER BY query_id DESC")
        results = self.c.fetchall()
        for i, q in enumerate(results):
            if i == 0:
                query_id = q[0]
                query = q[1]
                break
                
        query = (re.sub(r'[^\w\s]',' ',query)).strip() #remove the punctuations
        query = (re.sub('\d+',' ',query)).strip() #remove the numbers
        
        stop_words = set(stopwords.words('english')) #load stopping words txt
        lemmatizer = Lemmatizer() #set lemmatizer
  
        tokens = word_tokenize(query) #tokenization
        terms = []
        for w in tokens:
            lowerword = w.lower() #case folding
            if lowerword not in stop_words: #stopping
                term = lemmatizer.RunLemmatizer(lowerword).decode('utf8') #normalisation
                terms.append(term)
            else:
                print ('Sorry. There is a stopword in your query: "{}".'.format(lowerword))
                break
                
        self.query_terms = terms
        self.query_id = query_id
    
    def weight(self, term, doc): 
        w = 0
        try:
            tf = len(self.index[term][doc])
            df = len(self.index[term].keys())
            w = (math.log(self.N/df, 10))*(1+(math.log(tf, 10)))
        except:
            pass
        return w
    
    def TFIDF_rank(self):
        # compute and store relevency scores of docs 
        docs_query = []
        for term in self.query_terms:
            if term in self.index.keys():
                docs_term = self.index[term]
                for doc in docs_term:
                    if doc not in docs_query:
                        docs_query.append(doc)
            else:
                print ('Sorry. There is a word which is not in the database: "{}".'.format(term))
        scores = {}   
        for doc in docs_query:
            scores.setdefault(doc, 0)
            for term in self.query_terms:
                w = self.weight(term, doc)
                scores[doc] += w
            print (scores[doc])
        scores = sorted(scores.items(), key=lambda kv:kv[1], reverse=True)
        self.scores = scores
         
    def query_expansion(self, n):
        
        max_score = self.scores[0][1]
        min_score = max_score*0.5
            
        d_n = len(self.scores)    
        min_d_n = d_n*0.5
        
        relevant_docs = []
        for i, item in enumerate(self.scores):
            if item[1]>=min_score:
                relevant_docs.append(item[0])
            else:
                break
        
        if len(relevant_docs)<=min_d_n:
            for item in self.scores[i:]:
                relevant_docs.append(item[0])
        else:
            pass
        
        relevant_terms = []
        for docs_term in self.docs_terms:
            for doc in relevant_docs:
                if doc in docs_term:
                    relevant_terms.append(docs_term[0])
        
        scores_terms = {}
        for term in relevant_terms:
             scores_terms.setdefault(term, 0)
             for doc in relevant_docs:
                 w = self.weight(term, doc)
                 scores_terms[term] += w
        t_n = n + len(self.query_terms)
        scores_terms = sorted(scores_terms.items(), key=lambda kv:kv[1], reverse=True)[:t_n]
        query_expansion = []
        for i, score_term in enumerate(scores_terms):
            if score_term[0] not in self.query_terms:
                query_expansion.append(score_term[0])
        self.query_expansion = query_expansion[0:n]
        
    def main(self):
        self.TFIDF_rank()
        #self.query_expansion(3)
        self.c.execute("SELECT rank FROM search_result")
        results = self.c.fetchall()
        if len(results)>0:
            self.c.execute("TRUNCATE TABLE search_result")    
        length = len(self.scores)
        for i in range(length):
            rank = i+1
            docno = int(self.scores[i][0])
            relevance_score = self.scores[i][1]
            self.c.execute("INSERT INTO search_result (rank,doc_no,relevance) \
                           VALUES ('%d','%d','%f')"%(rank,docno,relevance_score))
        #self.c.execute("UPDATE query SET processed_query = '%s' WHERE query_id = '%d'" % (','.join(self.query_expansion),self.query_id))
        self.conn.commit()            
        self.conn.close()
        
IR = IR()
IR.main()

