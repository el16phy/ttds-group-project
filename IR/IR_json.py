#! C:/Users/Lydia/Anaconda3/python.exe
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 20 14:14:28 2018

@author: Lydia
"""
import re
import math
import json
import pymysql
from nltk import word_tokenize
from nltk.corpus import stopwords
from Lemmatizer_method import Lemmatizer

class IR():
    
    def __init__(self):
        
        self.N = 50000
        
        with open('50000_term_pos.json','r', encoding='utf-8') as load1:
            self.index = json.load(load1)
        with open('50000_term_doc.json','r', encoding='utf-8') as load2:
            self.docs_terms = json.load(load2)
        
        self.conn = pymysql.connect("localhost", "root", "root", "ttds", charset='utf8' )
        self.c = self.conn.cursor()
    
        self.query_preprocess()
           
    def query_preprocess(self):
        # read the newest query from database
        self.c.execute("SELECT query_id,original_query FROM query ORDER BY query_id DESC")
        results = self.c.fetchall()
        for i, q in enumerate(results):
            if i == 0:
                query_id = q[0]
                query = q[1]
                break
                
        query = (re.sub(r'[^\w\s]',' ',query)).strip() #remove the punctuations
        query = (re.sub('\d+',' ',query)).strip() #remove the numbers
        
        stop_words = set(stopwords.words('english')) #load stopping words txt
        lemmatizer = Lemmatizer() #set lemmatizer
  
        tokens = word_tokenize(query) #tokenization
        terms = []
        for w in tokens:
            lowerword = w.lower() #case folding
            if lowerword not in stop_words: #stopping
                term = lemmatizer.RunLemmatizer(lowerword).decode('utf8') #normalisation
                terms.append(term)
            else:
                print ('Sorry. There is a stopword in your query: "{}".'.format(lowerword))
                break     
        self.query_terms = terms
        self.query_id = query_id
    
    def weight(self, term, doc): 
        w = 0
        try:
            tf = len(self.index[term][doc])
            df = len(self.index[term].keys())
            w = (math.log(self.N/df, 10))*(1+(math.log(tf, 10)))
        except:
            pass
        return w
    
    def TFIDF_rank(self):
        # compute and store relevency scores of docs 
        docs_query = []
        for term in self.query_terms:
            print (term)
            if term in self.index.keys():
                docs_term = self.index[term]
                for doc in docs_term.keys():
                    if doc not in docs_query:
                        docs_query.append(doc)    
            else:
                print ('Sorry. There is a word which is not in the database: "{}".'.format(term))
        print (docs_query)
        scores = {}   
        for doc in docs_query:
            scores.setdefault(doc, 0)
            for term in self.query_terms:
                w = self.weight(term, doc)
                scores[doc] += w
        scores = sorted(scores.items(), key=lambda kv:kv[1], reverse=True)
        self.scores = scores
         
    def query_expansion(self, n):
        
        max_score = self.scores[0][1]
        min_score = max_score*0.85
            
        d_n = len(self.scores)    
        min_d_n = d_n*0.001
        
        relevant_docs = []
        for i, item in enumerate(self.scores):
            if item[1]>=min_score:
                relevant_docs.append(item[0])
            else:
                break
        
        if len(relevant_docs)<=min_d_n:
            for item in self.scores[i:]:
                relevant_docs.append(item[0])
                if len(relevant_docs)>=min_d_n:
                    break
        else:
            pass
        print (relevant_docs)
        relevant_terms = []
        for term in self.docs_terms.keys():
            docs_term = self.index[term]
            for doc in relevant_docs:
                if doc in docs_term.keys():
                    relevant_terms.append(term)

        scores_terms = {}
        for term in relevant_terms:
             scores_terms.setdefault(term, 0)
             for doc in relevant_docs:
                 w = self.weight(term, doc)
                 scores_terms[term] += w
        t_n = n + len(self.query_terms)
        scores_terms = sorted(scores_terms.items(), key=lambda kv:kv[1], reverse=True)[:t_n]
        query_expansion = []
        for i, score_term in enumerate(scores_terms):
            if score_term[0] not in self.query_terms:
                query_expansion.append(score_term[0])
        self.expansion_terms = query_expansion[0:n]
        
    def main(self):
        self.TFIDF_rank()
        self.query_expansion(3)
        self.c.execute("SELECT rank FROM search_result")
        results = self.c.fetchall()
        if len(results)>0:
            self.c.execute("TRUNCATE TABLE search_result")    
        length = len(self.scores)
        for i in range(length):
            rank = i+1
            docno = int(self.scores[i][0])
            relevance_score = self.scores[i][1]
            self.c.execute("INSERT INTO search_result (rank,doc_no,relevance) \
                           VALUES ('%d','%d','%f')"%(rank,docno,relevance_score))
        self.c.execute("UPDATE query SET processed_query = '%s' WHERE query_id = '%d'" % (','.join(self.expansion_terms),self.query_id))
        self.conn.commit()            
        self.conn.close()
        print (self.expansion_terms)
        
IR = IR()
IR.main()

