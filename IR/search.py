# -*- coding: utf-8 -*-
"""
Created on Fri Oct 19 12:55:36 2018

@author: Lydia
"""
import re
from nltk import word_tokenize
from nltk.stem import PorterStemmer

#to read the index file 
def read(file):
    file = open(file, 'r')
    return file

#to record all doc ID
def allID(indexfile):
    index = read(indexfile)
    lines = index.readlines()
    allID = []
    for line in lines:
        if re.match('\t', line) is not None:
            docid = re.sub('\t', '', line.split(":")[0])
            allID.append(docid)
    allID = sorted(list(set(allID)))
    print (allID)
    return allID
    
#just search for one word
def termsearch(indexfile, query):
    #preprocess our query
    index = read(indexfile)
    stemmer = PorterStemmer() #choose PorterStemmer as our stemmer
    query = query.lower()
    query = stemmer.stem(query)     
    #find the term in our index
    lines = index.readlines()
    i = 1
    for line in lines:       
        if re.match(query,line) is not None and line[len(query)==':']:
            break
        i = i + 1
    #creat a dictionary to record positions of the term
    term_pos = {}
    for line in lines[i:]:
        if re.match('\t', line) is None:
            break
        else:
            docid = re.sub('\t', '', line.split(":")[0])
            term_pos.setdefault(docid, [])
            positions = line.split(":")[1]
            positions = re.sub(r'[ \n]', '', positions)
            positions = positions.split(',')
            term_pos[docid]=positions
    docid = sorted(list(term_pos.keys()))
    return term_pos, docid
    
#search for phrase
def phrasesearch(indexfile, query):
    term1 = word_tokenize(query.split(' ', 1)[0])
    term2 = word_tokenize(query.split(' ', 1)[1])
    for t1 in term1:
        search1 = termsearch(indexfile, t1)            
        term_pos1 = search1[0]
        docid1 = search1[1]  
    for t2 in term2:  
        search2 = termsearch(indexfile, t2)            
        term_pos2 = search2[0]
        docid2 = search2[1] 
    docID = sorted(list(set(docid1).intersection(set(docid2))))
    docid = []
    for i in docID:
        for ii in term_pos1[i]:
            iii = str(int(ii)+1)
            if (iii in term_pos2[i]) is True:
                if i not in docid:
                    docid.append(i)
    return docid
    
#search for boolean queries
def booleansearch(indexfile, query):
    #preprocess our query
    query = re.sub(r'[^\w\s]', ' ', query)
    if (re.search(' AND ', query) is not None) and (re.search(' NOT ', query) is None):   
        #split our query into two terms
        term1 = word_tokenize(query.split(' AND ', 1)[0])
        term2 = word_tokenize(query.split(' AND ', 1)[1])
        if len(term1)==1 and len(term2)==1:        
            for t1 in term1:          
                docid1 = termsearch(indexfile, t1)[1]     
            for t2 in term2:   
                docid2 = termsearch(indexfile, t2)[1]
        elif len(term1)==1:           
            for t1 in term1:  
                docid1 = termsearch(indexfile, t1)[1]
            term2 = ' '.join(term1)
            docid2 = phrasesearch(indexfile, term2)
        elif len(term2)==1: 
            term1 = ' '.join(term1)
            docid1 = phrasesearch(indexfile, term1)
            for t2 in term2:  
                docid2 = termsearch(indexfile, t2)[1]
        else:
            term1 = ' '.join(term1)
            docid1 = phrasesearch(indexfile, term1)
            term2 = ' '.join(term2)
            docid2 = phrasesearch(indexfile, term2)
        docid = sorted(list(set(docid1).intersection(set(docid2))))
    elif re.search(' AND NOT ', query) is not None:
        term1 = word_tokenize(query.split(' AND NOT ', 1)[0])
        term2 = word_tokenize(query.split(' AND NOT ', 1)[1])
        if len(term1)==1 and len(term2)==1:        
            for t1 in term1:            
                docid1 = termsearch(indexfile, t1)[1]     
            for t2 in term2:   
                docid2 = termsearch(indexfile, t2)[1]
        elif len(term1)==1:           
            for t1 in term1:  
                docid1 = termsearch(indexfile, t1)[1]
            term2 = ' '.join(term1)
            docid2 = phrasesearch(indexfile, term2)
        elif len(term2)==1: 
            term1 = ' '.join(term1)
            docid1 = phrasesearch(indexfile, term1)
            for t2 in term2:  
                docid2 = termsearch(indexfile, t2)[1]
        else:
            term1 = ' '.join(term1)
            docid1 = phrasesearch(indexfile, term1)
            term2 = ' '.join(term1)
            docid2 = phrasesearch(indexfile, term2)
        docid = sorted(list(set(docid1).difference(set(docid2))))
    elif (re.search(' OR ', query) is not None) and (re.search(' NOT ', query) is None):
        term1 = word_tokenize(query.split(' OR ', 1)[0])
        term2 = word_tokenize(query.split(' OR ', 1)[1])
        if len(term1)==1 and len(term2)==1:        
            for t1 in term1:            
                docid1 = termsearch(indexfile, t1)[1]     
            for t2 in term2:   
                docid2 = termsearch(indexfile, t2)[1]
        elif len(term1)==1:           
            for t1 in term1:  
                docid1 = termsearch(indexfile, t1)[1]
            term2 = ' '.join(term1)
            docid2 = phrasesearch(indexfile, term2)
        elif len(term2)==1: 
            term1 = ' '.join(term1)
            docid1 = phrasesearch(indexfile, term1)
            for t2 in term2:  
                docid2 = termsearch(indexfile, t2)[1]
        else:
            term1 = ' '.join(term1)
            docid1 = phrasesearch(indexfile, term1)
            term2 = ' '.join(term1)
            docid2 = phrasesearch(indexfile, term2)
        docid = sorted(list(set(docid1).union(set(docid2))))
    elif re.search(' OR NOT ', query) is not None:
        term1 = word_tokenize(query.split(' OR NOT ', 1)[0])
        term2 = word_tokenize(query.split(' OR NOT', 1)[1])
        U = allID(indexfile)
        if len(term1)==1 and len(term2)==1:        
            for t1 in term1:           
                docid1 = termsearch(indexfile, t1)[1]     
            for t2 in term2:   
                docid2 = termsearch(indexfile, t2)[1]
        elif len(term1)==1:           
            for t1 in term1:  
                docid1 = termsearch(indexfile, t1)[1]
            term2 = ' '.join(term1)
            docid2 = phrasesearch(indexfile, term2)
        elif len(term2)==1: 
            term1 = ' '.join(term1)
            docid1 = phrasesearch(indexfile, term1)
            for t2 in term2:  
                docid2 = termsearch(indexfile, t2)[1]
        else:
            term1 = ' '.join(term1)
            docid1 = phrasesearch(indexfile, term1)
            term2 = ' '.join(term1)
            docid2 = phrasesearch(indexfile, term2)
        docid3 = list(set(U).difference(set(docid2)))
        docid = sorted(list(set(docid1).union(set(docid3))))
    return docid

#search for proximity queries
def proximity(indexfile, query):
    query = re.sub(r'[^\w\s]', ' ', query)
    query = word_tokenize(query)
    distance = int(query[0])
    #search the first term
    t1=query[1]
    search1 = termsearch(indexfile, t1)            
    term_pos1 = search1[0]
    docid1 = search1[1]
    #search the second term
    t2=query[2]  
    search2 = termsearch(indexfile, t2)            
    term_pos2 = search2[0]
    docid2 = search2[1] 
    #record the doc in which we can find both the two terms
    docID = sorted(list(set(docid1).intersection(set(docid2))))
    docid = []
    for d in range(1, distance+1):
        for i in docID:
            for pos1 in term_pos1[i]:
                pos2_1 = int(pos1)+d
                pos2_2 = int(pos1)-d
                for pos2 in term_pos2[i]:
                    if int(pos2)==pos2_1 or int(pos2)==pos2_2:
                        docid.append(i)
    docid = sorted(list(set(docid)))
    return docid

def main(indexfile, queryfile, resultfile):
    queries = read(queryfile)
    lines = queries.readlines()
    doc = open(resultfile, 'w')
    for line in lines:   
        query = word_tokenize(line)
        if len(query) == 2:
            docid = termsearch(indexfile, query[1])[1]
            for i in docid:
                print (query[0], '0', i, '0 1 0', file = doc)
        elif (re.search(' OR ', line) is not None) or (re.search(' AND ', line) is not None):
            q = line[2:]
            docid = booleansearch(indexfile, q)
            for i in docid:
                print (query[0], '0', i, '0 1 0', file = doc)      
        elif re.search('#', line) is not None:
            q = line[2:]
            docid = proximity(indexfile, q)
            for i in docid:
                print (query[0], '0', i, '0 1 0', file = doc)
        else:
            q = re.sub(r'[^\w\s]', ' ', line)
            q = word_tokenize(q)
            q = (' '. join(q))[2:]
            docid = phrasesearch(indexfile, q)
            for i in docid:
                print (query[0], '0', i, '0 1 0', file = doc)
                
main('index.txt', 'queries.boolean.txt', 'results.boolean.txt')

#or you can run like following for unstructured results of each query
#print(termsearch('index.txt','Edinburgh')[1])
#print(phrasesearch('index.txt',"income taxes"))
#print(proximity('index.txt','#20(income, taxes)'))
#print(booleansearch('index.txt','"wall street" AND dow'))
