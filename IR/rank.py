# -*- coding: utf-8 -*-
"""
Created on Sat Oct 20 14:14:28 2018

@author: Lydia
"""
import re
import math
import sqlite3
from nltk import word_tokenize
from nltk.corpus import stopwords
from Lemmatizer_method import Lemmatizer

class rank():
    
    def __init__(self):
        
        index = open('index.txt', 'r', encoding='utf-8')
        self.lines = index.readlines()
        index.close()
        
        self.conn = sqlite3.connect('data.db')
        self.c = self.conn.cursor()
        
    def query_preprocess(self):
        # read the newest query from database
        cursor = self.c.execute("SELECT contents FROM query ORDER BY id DESC")
        for i, q in enumerate(cursor):
            if i == 0:
                query = q[0]
                break
                
        query = (re.sub(r'[^\w\s]',' ',query)).strip() #remove the punctuations
        query = (re.sub('\d+',' ',query)).strip() #remove the numbers
        
        stop_words = set(stopwords.words('english')) #load stopping words txt
        lemmatizer = Lemmatizer() #set lemmatizer
  
        tokens = word_tokenize(query) #tokenization
        terms = []
        for w in tokens:
            lowerword = w.lower() #case folding
            if lowerword not in stop_words: #stopping
                term = lemmatizer.RunLemmatizer(lowerword).decode('utf8') #normalisation
                terms.append(term)
            #queryid = terms[0]
            #query = terms[1:] 
        self.query_terms = terms
    
    def weight(self, term): #to find a term in our index and compute its w for every docs it has appeared
        #to count the number of documents in a collection
        allID = []
        for line in self.lines:
            if re.match('\t', line) is not None:
                docid = re.sub('\t', '', line.split(":")[0])
                allID.append(docid)
        allID = set(allID)
        N = len(allID)
        #find the term in our index and record its pos in this file
        i = 1
        for line in self.lines:
            if re.match(term, line) is not None and line[len(term)==':']:
                break
            else:
                i = i + 1
        #create a dictionary to record the number of times the term appeared in every certain document
        term_pos_tf = {}
        for line in self.lines[i:]:
            if re.match('\t', line) is None:
                break
            else:
                docid = re.sub('\t', '', line.split(":")[0])
                term_pos_tf.setdefault(docid, [])
                positions = line.split(":")[1]
                positions = re.sub(r'[ \n]', '', positions)
                positions = positions.split(',')
                term_pos_tf[docid]=len(positions)
        # to count the number of documents the term appeared in
        docid = set(term_pos_tf.keys())
        df = len(docid)
        #create a dictionary to record the w(t,d)
        term_pos_w = {}
        for i in (list(term_pos_tf.keys())):
            term_pos_w.setdefault(i, [])
            w = (math.log(N/df, 10))*(1+(math.log(term_pos_tf[i], 10)))
            term_pos_w[i] = w
        return term_pos_w
    
    def TFIDF(self): # compute and store relevency scores of docs
        score = {} 
        for t in self.query_terms:
            w = self.weight(t)
            for i in w.keys():
                if i not in score.keys():
                    score.setdefault(i, [])
                    score[i] = w[i]
                else:
                    score[i] = score[i] + w[i]
        self.score = sorted(score.items(), key=lambda kv: kv[1], reverse=True)
        
    def main(self):
        
        self.query_preprocess()
        self.TFIDF()
        self.c.execute("DELETE FROM rank_result")    
        
        length = len(self.score)
        for i in range(length):
            rank = i+1
            docno = int(self.score[i][0])
            relevance_score = self.score[i][1]
            self.c.execute("INSERT INTO rank_result (rank,doc_no,difficulty,relevance) \
                      VALUES ('%d','%d', 0,'%f')"%(rank,docno,relevance_score)) 
        self.conn.commit()            
        self.conn.close()
        
        
#r = rank()
#r.main()

