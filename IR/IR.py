# -*- coding: utf-8 -*-
"""
Created on Sat Oct 20 14:14:28 2018

@author: Lydia
"""
import re
import math
import sqlite3
from nltk import word_tokenize
from nltk.corpus import stopwords
from Lemmatizer_method import Lemmatizer

class IR():
    
    def __init__(self):
        
        index = open('index.txt', 'r', encoding='utf-8')
        self.lines = index.readlines()
        index.close()
        
        self.conn = sqlite3.connect('data.db')
        self.c = self.conn.cursor()
        
        self.index_preprocess()
        self.query_preprocess()
    
    def index_preprocess(self):
        # to count the number of documents in a collection
        allID = []
        for line in self.lines:
            if re.match('\t', line) is not None:
                docid = re.sub('\t', '', line.split(":")[0])
                allID.append(docid)
        allID = set(allID)
        self.N = len(allID)
        # to store the index into dictionary 
        terms = []
        docs_terms_pos = []
        docs_terms = []
        for i, line in enumerate(self.lines):
            if re.match('\t', line) is None:
                docs_term_pos = {}
                docs_term = []
                term = line.split(":")[0]
                terms.append(term) 
                docs_term.append(term)
            if re.match('\t', line) is not None:               
                docid = re.sub('\t', '', line.split(":")[0])
                docs_term_pos.setdefault(docid, [])
                docs_term.append(docid)
                term_pos = re.sub('\n', '', line.split(":")[1]).split(", ")
                docs_term_pos[docid]=term_pos 
                if docs_term_pos not in docs_terms_pos:
                    docs_terms_pos.append(docs_term_pos)
                if docs_term not in docs_terms:
                    docs_terms.append(docs_term)
        self.index = dict(zip(terms,docs_terms_pos))
        self.docs_terms = docs_terms
           
    def query_preprocess(self):
        # read the newest query from database
        cursor = self.c.execute("SELECT id,contents FROM query ORDER BY id DESC")
        for i, q in enumerate(cursor):
            if i == 0:
                query_id = q[0]
                query = q[1]
                break
                
        query = (re.sub(r'[^\w\s]',' ',query)).strip() #remove the punctuations
        query = (re.sub('\d+',' ',query)).strip() #remove the numbers
        
        stop_words = set(stopwords.words('english')) #load stopping words txt
        lemmatizer = Lemmatizer() #set lemmatizer
  
        tokens = word_tokenize(query) #tokenization
        terms = []
        for w in tokens:
            lowerword = w.lower() #case folding
            if lowerword not in stop_words: #stopping
                term = lemmatizer.RunLemmatizer(lowerword).decode('utf8') #normalisation
                terms.append(term)
                
        self.query_terms = terms
        self.query_id = query_id
    
    def weight(self, term, doc): 
        w = 0
        try:
            tf = len(self.index[term][doc])
            df = len(self.index[term].keys())
            w = (math.log(self.N/df, 10))*(1+(math.log(tf, 10)))
        except:
            pass
        return w
    
    def TFIDF_rank(self):
        # compute and store relevency scores of docs 
        docs_query = []
        for term in self.query_terms:
            docs_term = self.index[term]
            for doc in docs_term:
                if doc not in docs_query:
                    docs_query.append(doc)       
        scores = {}   
        for doc in docs_query:
            scores.setdefault(doc, 0)
            for term in self.query_terms:
                w = self.weight(term, doc)
                scores[doc] += w
        self.scores = sorted(scores.items(), key=lambda kv:kv[1], reverse=True)
        self.relevant_docs = docs_query
        #print (self.score)
        #print(self.query_terms)
         
    def query_expension(self, n):
        relevant_terms = []
        for docs_term in self.docs_terms:
            for doc in self.relevant_docs:
                if doc in docs_term:
                    relevant_terms.append(docs_term[0])
        scores_terms = {}
        for term in relevant_terms:
             scores_terms.setdefault(term, 0)
             for doc in self.relevant_docs:
                 w = self.weight(term, doc)
                 scores_terms[term] += w
        scores_terms = sorted(scores_terms.items(), key=lambda kv:kv[1], reverse=True)[:n]
        new_query_terms = self.query_terms
        query_expension = ''
        for i, score_term in enumerate(scores_terms):
            new_query_terms.append(score_term[0])
            if query_expension == '':
                query_expension = score_term[0]
            else:    
                query_expension = query_expension + ',' + score_term[0]           
        self.new_query_terms = new_query_terms
        self.query_expension = query_expension
        
    def main(self):
        self.TFIDF_rank()
        self.query_expension(3)
        self.c.execute("DELETE FROM rank_result")    
        length = len(self.scores)
        for i in range(length):
            rank = i+1
            docno = int(self.scores[i][0])
            relevance_score = self.scores[i][1]
            self.c.execute("INSERT INTO rank_result (rank,doc_no,difficulty,relevance) \
                           VALUES ('%d','%d', 0,'%f')"%(rank,docno,relevance_score)) 
        self.c.execute("INSERT INTO query_expension (id,terms) \
                       VALUES ('%d','%s')"%(self.query_id,self.query_expension)) 
        self.conn.commit()            
        self.conn.close()
        
        
IR = IR()
IR.main()
