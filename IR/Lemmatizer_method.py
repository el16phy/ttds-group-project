from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag, word_tokenize

"""
Created on 4, Jan, 2019

@author: Po-Hung Yeh
@version: 1.0
@Purpose: Lemmatized the string value
"""

class Lemmatizer:
    def __init__(self):
        # Convert all the tags to wordnet form
        self.pos_to_wornet_dict = {

            'JJ': wordnet.ADJ,
            'JJR': wordnet.ADJ,
            'JJS': wordnet.ADJ,
            'RB': wordnet.ADV,
            'RBR': wordnet.ADV,
            'RBS': wordnet.ADV,
            'NN': wordnet.NOUN,
            'NNP': wordnet.NOUN,
            'NNS': wordnet.NOUN,
            'NNPS': wordnet.NOUN,
            'VB': wordnet.VERB,
            'VBG': wordnet.VERB,
            'VBD': wordnet.VERB,
            'VBN': wordnet.VERB,
            'VBP': wordnet.VERB,
            'VBZ': wordnet.VERB,

        }

        self.lemmatizer = WordNetLemmatizer() # init the Lemmatizer

    def RunLemmatizer(self, sentence):
        '''
        @Last Edited on 5, Jan, 2019
        @Last Editor : Po-Hung Yeh
        @purpose:
            The function will receive a input string and reuturn the lemmatized string

        @arg:
            (string) sentence: the input string value
        @return:
            (string) str1: the output string value after lemmatizing

        '''

        word = ""
        ReturnWord = [] # stores return words
        tagged = pos_tag(word_tokenize(sentence)) # tag the word with different form(verb, noun, adj, adv)

        for i in tagged:
            if i[1] in self.pos_to_wornet_dict:
                word = (self.lemmatizer.lemmatize(i[0], pos= (self.pos_to_wornet_dict[i[1]]))) #the word has been Lemmatize
            else:
                word = i[0] #original word without changing
            ReturnWord.append(word)
        str1 = ' '.join(ReturnWord) # return the words as string
        return str1.encode("utf-8")

