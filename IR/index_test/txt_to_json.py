# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 09:29:04 2019

@author: Lydia
"""
import re
import json


index = open('index_5000_10000', 'r', encoding='utf-8')
lines = index.readlines()
index.close()

terms = []
docs_terms_pos = []
docs_terms = []
for line in lines:
    if re.match('\t', line) is None:
        docs_term_pos = {}
        docs_term = []
        term = line.split(":")[0]
        terms.append(term) 
        docs_term.append(term)
        print (term)
    if re.match('\t', line) is not None:
        docid = re.sub('\t', '', line.split(":")[0])
        docid = re.sub('\t', '', line.split(":")[0])
        docs_term_pos.setdefault(docid, [])
        docs_term.append(docid)
        term_pos = re.sub('\n', '', line.split(":")[1]).split(", ")
        docs_term_pos[docid]=term_pos 
        if docs_term_pos not in docs_terms_pos:
            docs_terms_pos.append(docs_term_pos)
        if docs_term not in docs_terms:
            docs_terms.append(docs_term)
filename='5000_10000'
with open(filename+'.json','a') as outfile:
    json.dump(dict(zip(terms,docs_terms_pos)),outfile,ensure_ascii=False)
    outfile.write('\n')
with open(filename+'_term'+'.json','a') as outfile:
    json.dump(docs_terms,outfile,ensure_ascii=False)
    outfile.write('\n')
