# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 06:25:12 2018

@author: Lydia
"""

import re
import xml.etree.ElementTree as ET
from nltk import word_tokenize
from nltk.corpus import stopwords
from Lemmatizer_method import Lemmatizer

class index():
    
    # to read the '.xml' file
    def readxml(self, file):
        #read the xml
        tree = ET.parse(file)
        root = tree.getroot()
        return root
    
    # to preprocess the '.xml' file
    def preproxml(self, root):
        
        stop_words = set(stopwords.words('english')) #load stopping words
             
        lemmatizer = Lemmatizer() #set lemmatizer
        docID = [] #list to store the docID
        doc = [] #list to store the documents
        
        for child in root:
            #process the xml    
            for item in child:
                item_att = item.tag
                if item_att == 'DOCNO':
                    docID.insert(len(docID), item.text)
                    print (item.text)
                if item_att == 'Title':
                    headline = item.text
                if item_att == 'Text':
                    words = headline + ' ' + item.text
                    words = (re.sub(r'[^\w\s]',' ',words)).strip() #remove the punctuations
                    words = (re.sub('\d+',' ',words)).strip() #remove the numbers
                    tokens = word_tokenize(words) #tokenization
                    terms = []
                    for w in tokens:
                        lowerword = w.lower() #case folding
                        if lowerword not in stop_words: #stopping
                            term = lemmatizer.RunLemmatizer(lowerword).decode('utf8') #normalisation
                            terms.append(term)
                    doc.insert(len(doc), terms)
        return docID, doc

    #the inverted index
    def inverted_index(self, docID, doc):
        #store positions
        terms_pos = {} 
        for i, content in enumerate(doc):
            for pos, term in enumerate(content):
                docid = docID[i]
                terms_pos.setdefault(term, [])
                if docid not in terms_pos[term]:
                    terms_pos[term].append(docid)
        #sort
        terms_order_pos = {}
        for key in sorted(terms_pos.keys()):
            terms_order_pos[key] = terms_pos[key]  
        return terms_order_pos
    
    #the positional inverted index
    def index(self, docID, doc, terms_order_pos):
        documents = dict(zip(docID, doc))
        terms_pos = {}
        doc = open ('index.txt', 'w', encoding='utf-8')
        for term in terms_order_pos.keys():
            terms_pos.setdefault(term, [])
            print ('{}:'.format(term), file = doc)
            for i, docid in enumerate(terms_order_pos[term]):
                positions = []
                for j, content in enumerate(documents[docid]):
                    if content==term:
                        positions.append(j+1)
                term_pos = docid+':'+positions.__str__()
                term_pos = re.sub('\[', ' ',term_pos)
                term_pos = re.sub('\]', '',term_pos)
                terms_pos[term].append(term_pos)  
                print ('\t{}'.format(term_pos), file = doc)     
        doc.close()
        return terms_pos
        

a = index()
root = a.readxml('simple.xml')
doc = a.preproxml(root)
index = a.inverted_index(doc[0], doc[1])
a.index(doc[0],doc[1],index)














