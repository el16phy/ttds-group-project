# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 22:46:30 2019

@author: Lydia
"""

import re
import pymysql
import json
from nltk import word_tokenize
from nltk.corpus import stopwords
from Lemmatizer_method import Lemmatizer

class index_append():
    
    # to initialize
    def __init__(self):
        
        self.conn = pymysql.connect("localhost", "root", "root", "ttds", charset='utf8' )
        self.c = self.conn.cursor()
        
        with open('50000_term_pos.json','r') as load_f:
            self.term_pos = json.load(load_f)
        with open('50000_term_doc.json','r') as load_f:
            self.term_doc = json.load(load_f)
            
    # to preprocess the text from database
    def preprodata(self):
        
        stop_words = set(stopwords.words('english')) #load stopping words
             
        lemmatizer = Lemmatizer() #set lemmatizer
        docID = [] #list to store the docID
        doc = [] #list to store the documents
        
        self.c.execute("SELECT docid,doctitle,doccontent FROM doc")
        results = self.c.fetchall()
        
        for item in results[50000:]: # mark the start point
            docID.insert(len(docID), item[0])
            print (item[0])
            words = item[1]+'\t'+item[2]
            words = (re.sub(r'[^\w\s]',' ',words)).strip() #remove the punctuations
            words = (re.sub('\d+',' ',words)).strip() #remove the numbers
            tokens = word_tokenize(words) #tokenization
            terms = []
            for w in tokens:
                if not any(ord(i)>127 for i in w):
                    lowerword = w.lower() #case folding
                if lowerword not in stop_words: #stopping
                    term = lemmatizer.RunLemmatizer(lowerword).decode('utf8') #normalisation
                    terms.append(term)
            doc.insert(len(doc), terms)               
        self.docID = docID
        self.doc = doc

    #the inverted index
    def inverted_index(self):
        #store positions
        terms_pos = {} 
        for i, content in enumerate(self.doc):
            for pos, term in enumerate(content):
                docid = self.docID[i]
                terms_pos.setdefault(term, [])
                if docid not in terms_pos[term]:
                    terms_pos[term].append(docid)
        #sort
        terms_order_pos = {}
        for key in sorted(terms_pos.keys()):
            terms_order_pos[key] = terms_pos[key]  
        
        self.terms_order_pos = terms_order_pos
    
    #the positional inverted index
    def main(self):
        self.preprodata()
        self.inverted_index()

        documents = dict(zip(self.docID, self.doc))
        term_pos = self.term_pos
        print (term_pos)
        term_doc = self.term_doc
        n = 0
        for term in self.terms_order_pos.keys():
            n += 1
            print ('loop:', n)
            term_pos.setdefault(term, {})
            term_doc.setdefault(term, [])
            for i, docid in enumerate(self.terms_order_pos[term]):
                term_doc[term].append(docid)
                positions = term_pos[term]
                positions.setdefault(docid, [])
                for j, content in enumerate(documents[docid]):
                    if content==term:
                        positions[docid].append(j+1)
            term_pos[term] = positions
        with open('new_term_pos'+'.json','a') as outfile:        
            json.dump(term_pos,outfile,ensure_ascii=False)
        with open('new_term_doc'+'.json','a') as outfile:        
            json.dump(term_doc,outfile,ensure_ascii=False)
        

a = index_append()
a.main()