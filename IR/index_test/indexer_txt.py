# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 06:25:12 2018

@author: Lydia
"""

import re
import pymysql
from nltk import word_tokenize
from nltk.corpus import stopwords
from Lemmatizer_method import Lemmatizer

class index():
    
    # to initialize
    def __init__(self):
        
        self.conn = pymysql.connect("localhost", "root", "root", "ttds", charset='utf8' )
        self.c = self.conn.cursor()
        
    # to preprocess the text from database
    def preprodata(self):
        
        stop_words = set(stopwords.words('english')) #load stopping words
             
        lemmatizer = Lemmatizer() #set lemmatizer
        docID = [] #list to store the docID
        doc = [] #list to store the documents
        
        self.c.execute("SELECT docid,doctitle,doccontent FROM doc")
        results = self.c.fetchall()
        
        for item in results[0:10000]:
            docID.insert(len(docID), item[0])
            print (item[0])
            words = item[1]+'\t'+item[2]
            words = (re.sub(r'[^\w\s]',' ',words)).strip() #remove the punctuations
            words = (re.sub('\d+',' ',words)).strip() #remove the numbers
            tokens = word_tokenize(words) #tokenization
            terms = []
            for w in tokens:
                if not any(ord(i)>127 for i in w):
                    lowerword = w.lower() #case folding
                if lowerword not in stop_words: #stopping
                    term = lemmatizer.RunLemmatizer(lowerword).decode('utf8') #normalisation
                    terms.append(term)
            doc.insert(len(doc), terms)
                
        self.docID = docID
        self.doc = doc

    #the inverted index
    def inverted_index(self):
        #store positions
        terms_pos = {} 
        for i, content in enumerate(self.doc):
            for pos, term in enumerate(content):
                docid = self.docID[i]
                terms_pos.setdefault(term, [])
                if docid not in terms_pos[term]:
                    terms_pos[term].append(docid)
        #sort
        terms_order_pos = {}
        for key in sorted(terms_pos.keys()):
            terms_order_pos[key] = terms_pos[key]  
        
        self.terms_order_pos = terms_order_pos
    
    #the positional inverted index
    def main(self):
        self.preprodata()
        self.inverted_index()
        documents = dict(zip(self.docID, self.doc))
        terms_pos = {}
        index_doc = open('10000_index.txt', 'w', encoding='utf-8')
        n = 0
        for term in self.terms_order_pos.keys():
            n += 1
            print ('loop:', n)
            terms_pos.setdefault(term, [])
            print ('{}:'.format(term), file = index_doc)
            for i, docid in enumerate(self.terms_order_pos[term]):
                positions = []
                for j, content in enumerate(documents[docid]):
                    if content==term:
                        positions.append(j+1)
                term_pos = str(docid)+':'+positions.__str__()
                term_pos = re.sub('\[', '',term_pos)
                term_pos = re.sub('\]', '',term_pos)
                terms_pos[term].append(term_pos)  
                print ('\t{}'.format(term_pos), file = index_doc)     
        index_doc.close()
        

a = index()
a.main()
#a.preprodata()




