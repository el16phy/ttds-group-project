4
1
0
2

n
u

J

9

]

V

C

.

s

c

[

2
v
6
3
7
0

.

4
0
4
1

:

v

i

X

r

a

Exploiting Linear Structure Within Convolutional
Networks for Efﬁcient Evaluation

Emily Denton1 , Wojciech Zaremba1 , Joan Bruna1 , Yann LeCun1,2 and Rob Fergus1,2

1Dept. of Computer Science, Courant Institute, New York University
2Facebook AI Research

{denton, zaremba, bruna, lecun, fergus} @cs.nyu.edu

Abstract

We present techniques for speeding up the test-time evaluation of large convo-
lutional networks, designed for object recognition tasks. These models deliver
impressive accuracy, but each image evaluation requires millions of ﬂoating point
operations, making their deployment on smartphones and Internet-scale clusters
problematic. The computation is dominated by the convolution operations in the
lower layers of the model. We exploit the redundancy present within the con-
volutional ﬁlters to derive approximations that signiﬁcantly reduce the required
computation. Using large state-of-the-art models, we demonstrate speedups of
convolutional layers on both CPU and GPU by a factor of 2×, while keeping the
accuracy within 1% of the original model.

1

Introduction

Large neural networks have recently demonstrated impressive performance on a range of speech and
vision tasks. However, the size of these models can make their deployment at test time problematic.
For example, mobile computing platforms are limited in their CPU speed, memory and battery life.
At the other end of the spectrum, Internet-scale deployment of these models requires thousands
of servers to process the 100’s of millions of images per day. The electrical and cooling costs of
these servers is signiﬁcant. Training large neural networks can take weeks, or even months. This
hinders research and consequently there have been extensive efforts devoted to speeding up training
procedure. However, there are relatively few efforts aimed at improving the test-time performance
of the models.
We consider convolutional neural networks (CNNs) used for computer vision tasks, since they are
large and widely used in commercial applications. These networks typically require a huge number
of parameters (∼ 108 in [1]) to produce state-of-the-art results. While these networks tend to be
hugely over parameterized [2], this redundancy seems necessary in order to overcome a highly non-
convex optimization [3]. As a byproduct, the resulting network wastes computing resources. In this
paper we show that this redundancy can be exploited with linear compression techniques, resulting
in signiﬁcant speedups for the evaluation of trained large scale networks, with minimal compromise
to performance.
We follow a relatively simple strategy: we start by compressing each convolutional layer by ﬁnding
an appropriate low-rank approximation, and then we ﬁne-tune the upper layers until the prediction
performance is restored. We consider several elementary tensor decompositions based on singular
value decompositions, as well as ﬁlter clustering methods to take advantage of similarities between
learned features.

1

Our main contributions are the following: (1) We present a collection of generic methods to exploit
the redundancy inherent in deep CNNs.
CNNs, showing empirical speedups on convolutional layers by a factor of 2 − 3× and a reduction
(2) We report experiments on state-of-the-art Imagenet
of parameters in fully connected layers by a factor of 5 − 10×.
Notation: Convolution weights can be described as a 4-dimensional tensor: W ∈ RC×X×Y ×F . C
is the number of number of input channels, X and Y are the spatial dimensions of the kernel, and F
is the target number of feature maps. It is common for the ﬁrst convolutional layer to have a stride
associated with the kernel which we denote by ∆. Let I ∈ RC×N ×M denote an input signal where
C is the number of input maps, and N and M are the spatial dimensions of the maps. The target
value, T = I ∗ W , of a generic convolutional layer, with ∆ = 1, for a particular output feature, f ,
and spatial location, (x, y), is

C(cid:88)

X(cid:88)

Y(cid:88)

T (f , x, y) =

I (c, x − x(cid:48) , y − y (cid:48) )W (c, x(cid:48) , y (cid:48) , f )

c=1

x(cid:48)=1

y (cid:48)=1

If W is a tensor, (cid:107)W (cid:107) denotes its operator norm, sup(cid:107)x(cid:107)=1 (cid:107)W x(cid:107)F and (cid:107)W (cid:107)F denotes its Frobenius
norm.

2 Related Work

Vanhoucke et al. [4] explored the properties of CPUs to speed up execution. They present many
solutions speciﬁc to Intel and AMD CPUs, however some of their techniques are general enough to
be used for any type of processor. They describe how to align memory, and use SIMD operations
(vectorized operations on CPU) to boost the efﬁciency of matrix multiplication. Additionally, they
propose the linear quantization of the network weights and input. This involves representing weights
as 8-bit integers (range [−127, 128]), rather than 32-bit ﬂoats. This approximation is similar in spirit
to our approach, but differs in that it is applied to each weight element independently. By contrast,
our approximation approach models the structure within each ﬁlter. Potentially, the two approaches
could be used in conjunction.
The most expensive operations in CNNs are the convolutions in the ﬁrst few layers. The complexity
of this operation is linear in the area of the receptive ﬁeld of the ﬁlters, which is relatively large for
these layers. However, Mathieu et al. [5] have shown that convolution can be efﬁciently computed
in Fourier domain, where it becomes element-wise multiplication (and there is no cost associated
with size of receptive ﬁeld). They report a forward-pass speed up of around 2× for convolution
layers in state-of-the-art models. Importantly, the FFT method can be used jointly with most of the
techniques presented in this paper.
The use of low-rank approximations in our approach is inspired by work of Denil et al. [2] who
layer can be accurately predicted from a small (e.g. ∼ 5%) subset of them. This indicates that
demonstrate the redundancies in neural network parameters. They show that the weights within a
neural networks are heavily over-parametrized. All the methods presented here focus on exploiting
the linear structure of this over-parametrization.
Finally, a recent preprint [6] also exploits low-rank decompositions of convolutional tensors to speed
up the evaluation of CNNs, applied to scene text character recognition. This work was developed
simultaneously with ours, and provides further evidence that such techniques can be applied to a
variety of architectures and tasks. Out work differs in several ways. First, we consider a signiﬁcantly
larger model. This makes it more challenging to compute efﬁcient approximations since there are
more layers to propagate through and thus a greater opportunity for error to accumulate. Second, we
present different compression techniques for the hidden convolutional layers and provide a method
of compressing the ﬁrst convolutional layer. Finally, we present GPU results in addition to CPU
results.

3 Convolutional Tensor Compression

In this section we describe techniques for compressing 4 dimensional convolutional weight tensors
and fully connected weight matrices into a representation that permits efﬁcient computation and

2

storage. Section 3.1 describes how to construct a good approximation criteria. Section 3.2 describes
techniques for low-rank tensor approximations. Sections 3.3 and 3.4 describe how to apply these
techniques to approximate weights of a convolutional neural network.

3.1 Approximation Metric

Our goal is to ﬁnd an approximation, ˜W , of a convolutional tensor W that facilitates more efﬁcient
computation while maintaining the prediction performance of the network. A natural choice for
an approximation criterion is to minimize (cid:107) ˜W − W (cid:107)F . This criterion yields efﬁcient compression
schemes using elementary linear algebra, and also controls the operator norm of each linear convolu-
tional layer. However, this criterion assumes that all directions in the space of weights equally affect
prediction performance. We now present two methods of improving this criterion while keeping the
same efﬁcient approximation algorithms.
Mahalanobis distance metric: The ﬁrst distance metric we propose seeks to emphasize coordi-
nates more prone to produce prediction errors over coordinates whose effect is less harmful for the
overall system. We can obtain such measurements as follows. Let Θ = {W1 , . . . , WS } denote
the set of all parameters of the S -layer network, and let U (I ; Θ) denote the output after the soft-
max layer of input image I . We consider a given input training set (I1 , . . . , IN ) with known labels
(y1 , . . . , yN ). For each pair (In , yn ), we compute the forward propagation pass U (In , Θ), and de-
ﬁne as {βn} the indices of the h largest values of U (In , Θ) different from yn . Then, for a given
layer s, we compute

dn,l,s = ∇Ws (U (In , Θ) − δ(i − l)) , n ≤ N , l ∈ {βn} , s ≤ S ,

(1)
where δ(i− l) is the dirac distribution centered at l. In other words, for each input we back-propagate
the difference between the current prediction and the h “most dangerous” mistakes.
The Mahalanobis distance is deﬁned from the covariance of d: (cid:107)W (cid:107)2
is the vector containing all the coordinates of W , and Σ is the covariance of (dn,l,s )n,l . We do not
report results using this metric, since it requires inverting a matrix of size equal to the number of pa-
rameters, which can be prohibitively expensive in large networks. Instead we use an approximation
that considers only the diagonal of the covariance matrix. In particular, we propose the following,
approximate, Mahalanobis distance metric:

maha = wΣ−1wT , where w

(cid:16) (cid:88)

dn,l,s (p)2(cid:17)1/2

(2)

(cid:88)

(cid:107)W (cid:107)(cid:94)maha

:=

αpW (p) , where αp =

p

n,l

Ex∼N (0,I )(cid:107)W x(cid:107)2

Data covariance distance metric:

where the sum runs over the tensor coordinates. Since (2) is a reweighted Euclidiean metric, we can
simply compute W (cid:48) = α . ∗ W , where .∗ denotes element-wise multiplication, then compute the
approximation ˜W (cid:48) on W (cid:48) using the standard L2 norm, and ﬁnally output ˜W = α−1 . ∗ ˜W (cid:48) .
One can view the Frobenius norm of W as (cid:107)W (cid:107)2
RC×X×Y ×F is a convolutional layer, and (cid:98)Σ ∈ RCX Y ×CX Y is the empirical estimate of the input
F . Another alternative, similar to the one considered in [6], is to replace the
isotropic covariance assumption by the empirical covariance of the input of the layer.
If W ∈
data covariance, it can be efﬁciently computed as
(3)

(cid:107)W (cid:107)data = (cid:107) (cid:98)Σ1/2WF (cid:107)F ,

F =

where WF is the matrix obtained by folding the ﬁrst three dimensions of W .As opposed to [6], this
approach adapts to the input distribution without the need to iterate through the data.

3.2 Low-rank Tensor Approximations

3.2.1 Matrix Decomposition

Matrices are 2-tensors which can be linearly compressed using the Singular Value Decomposition.
If W ∈ Rm×k is a real matrix, the SVD is deﬁned as W = U SV (cid:62) , where U ∈ Rm×m , S ∈
Rm×k , V ∈ Rk×k . S is a diagonal matrix with the singular values on the diagonal, and U , V
are orthogonal matrices. If the singular values of W decay rapidly, W can be well approximated

3

by keeping only the t largest entries of S , resulting in the approximation ˜W = ˜U ˜S ˜V (cid:62) , where
˜U ∈ Rm×t , ˜S ∈ Rt×t , ˜V ∈ Rt×k Then, for I ∈ Rn×m , the approximation error (cid:107)I ˜W − IW (cid:107)F
satisﬁes (cid:107)I ˜W − IW (cid:107)F ≤ st+1(cid:107)I (cid:107)F , and thus is controlled by the decay along the diagonal of S .
Now the computation I ˜W can be done in O(nmt + nt2 + ntk), which, for sufﬁciently small t is
signiﬁcantly smaller than O(nmk).

3.2.2 Higher Order Tensor Approximations

SVD can be used to approximate a tensor W ∈ Rm×n×k by ﬁrst folding all but two dimensions
together to convert it into a 2-tensor, and then considering the SVD of the resulting matrix. For
example, we can approximate Wm ∈ Rm×(nk) as ˜Wm ≈ ˜U ˜S ˜V (cid:62) . W can be compressed even
further by applying SVD to ˜V . We refer to this approximation as the SVD decomposition and use
K1 and K2 to denote the rank used in the ﬁrst and second application of SVD respectively.
Alternatively, we can approximate a 3-tensor, WS ∈ Rm×n×k , by a rank 1 3-tensor by ﬁnding a
decomposition that minimizes
(4)
where α ∈ Rm , β ∈ Rn , γ ∈ Rk and ⊗ denotes the outer product operation. Problem (4) is solved
efﬁciently by performing alternate least squares on α, β and γ respectively, although more efﬁcient
algorithms can also be considered [7].
This easily extends to a rank K approximation using a greedy algorithm: Given a tensor W , we
compute (α, β , γ ) using (4), and we update W (k+1) ← W k − α ⊗ β ⊗ γ . Repeating this operation
K times results in

(cid:107)W − α ⊗ β ⊗ γ (cid:107)F ,

K(cid:88)

˜WS =

αk ⊗ βk ⊗ γk .

(5)

We refer to this approximation as the outer product decomposition and use K to denote the rank of
the approximation.

k=1

(a)

(b)

(c)

Figure 1: A visualization of monochromatic and biclustering approximation structures. (a) The
monochromatic approximation, used for the ﬁrst layer. Input color channels are projected onto a set
of intermediate color channels. After this transformation, output features need only to look at one
intermediate color channel. (b) The biclustering approximation, used for higher convolution layers.
Input and output features are clustered into equal sized groups. The weight tensor corresponding
to each pair of input and output clusters is then approximated.
(c) The weight tensors for each
input-output pair in (b) are approximated by a sum of rank 1 tensors using techniques described in
3.2.2

3.3 Monochromatic Convolution Approximation

Let W ∈ RC×X×Y ×F denote the weights of the ﬁrst convolutional layer of a trained network.
We found that the color components of trained CNNs tend to have low dimensional structure. In
particular, the weights can be well approximated by projecting the color dimension down to a 1D
subspace. The low-dimensional structure of the weights is illustrated in Figure 4.1.1.
The monochromatic approximation exploits this structure and is computed as follows. First, for
every output feature, f , we consider consider the matrix Wf ∈ RC×(X Y ) , where the spatial di-
mensions of the ﬁlter corresponding to the output feature have been combined, and ﬁnd the SVD,

4

Approximation technique

Number of operations

No approximation
Monochromatic
Biclustering + outer product decomposition GHK (N M C
Biclustering + SVD

X Y C F N M ∆−2
C (cid:48)CN M + X Y F N M ∆−2
G + X Y N M ∆−2 + F
H N M ∆−2 )
G K1 + K1X Y K2∆−2 + K2
GH N M ( C
H )

F

Table 1: Number of operations required for various approximation methods.

f

Wf = Uf Sf V (cid:62)

f , where Uf ∈ RC×C , Sf ∈ RC×X Y , and Vf ∈ RX Y ×X Y . We then take the rank
1 approximation of Wf , ˜Wf = ˜Uf ˜Sf ˜V (cid:62)
, where ˜Uf ∈ RC×1 , ˜Sf ∈ R, ˜Vf ∈ R1×X Y . We can
further exploit the regularity in the weights by sharing the color component basis between different
output features. We do this by clustering the F left singular vectors, ˜Uf , of each output feature f
into C (cid:48) clusters, for C (cid:48) < F . We constrain the clusters to be of equal size as discussed in section
3.4. Then, for each of the F
C (cid:48) output features, f , that is assigned to cluster cf , we can approximate
f where Ucf ∈ RC×1 is the cluster center for cluster cf and ˜Sf and ˜Vf are

Wf with ˜Wf = Ucf

as before.
This monochromatic approximation is illustrated in the left panel of Figure 1(c). Table 1 shows the
number of operations required for the standard and monochromatic versions.

˜Sf ˜V (cid:62)

3.4 Biclustering Approximations

We exploit the redundancy within the 4-D weight tensors in the higher convolutional layers by clus-
tering the ﬁlters, such that each cluster can be accurately approximated by a low-rank factorization.
We start by clustering the rows of WC ∈ RC×(X Y F ) , which results in clusters C1 , . . . , Ca . Then
we cluster the columns of WF ∈ R(CX Y )×F , producing clusters F1 , . . . , Fb . These two opera-
tions break the original weight tensor W into ab sub-tensors {WCi ,Fj }i=1,...,a,j=1,...,b as shown in
Figure 1(b). Each sub-tensor contains similar elements, and thus is easier to ﬁt with a low-rank
approximation.
In order to exploit the parallelism inherent in CPU and GPU architectures it is useful to constrain
clusters to be of equal sizes. We therefore perform the biclustering operations (or clustering for
monochromatic ﬁlters in Section 3.3) using a modiﬁed version of the k-means algorithm which bal-
ances the cluster count at each iteration. It is implemented with the Floyd algorithm, by modifying
the Euclidean distance with a subspace projection distance.
After the input and output clusters have been obtained, we ﬁnd a low-rank approximation of each
sub-tensor using either the SVD decomposition or the outer product decomposition as described in
Section 3.2.2. We concatenate the X and Y spatial dimensions of the sub-tensors so that the de-
composition is applied to the 3-tensor, WS ∈ RC×(X Y )×F . While we could look for a separable
approximation along the spatial dimensions as well, we found the resulting gain to be minimal. Us-
ing these approximations, the target output can be computed with signiﬁcantly fewer operations. The
number of operations required is a function the number of input clusters, G, the output clusters H
and the rank of the sub-tensor approximations (K1 , K2 for the SVD decomposition; K for the outer
product decomposition. The number of operations required for each approximation is described in
Table 1.

3.5 Fine-tuning

Many of the approximation techniques presented here can efﬁciently compress the weights of a
CNN with negligible degradation of classiﬁcation performance provided the approximation is not
too harsh. Alternatively, one can use a harsher approximation that gives greater speedup gains but
hurts the performance of the network. In this case, the approximated layer and all those below it can
be ﬁxed and the upper layers can be ﬁne-tuned until the original performance is restored.

5

Figure 2: Visualization of the 1st layer ﬁlters. (Left) Each component of the 96 7x7 ﬁlters is plotted
in RGB space. Points are colored based on the output ﬁlter they belong to. Hence, there are 96
colors and 72 points of each color. Leftmost plot shows the original ﬁlters and the right plot shows
the ﬁlters after the monochromatic approximation, where each ﬁlter has been projected down to a
line in colorspace. (Right) Original and approximate versions of a selection of 1st layer ﬁlters.

4 Experiments

We use the 15 layer convolutional architecture of [8], trained on the ImageNet 2012 dataset [9]. The
network contains 4 convolutional layers, 3 fully connected layers and a softmax output layer. We
evaluated the network on both CPU and GPU platforms. All measurements of prediction perfor-
mance are with respect to the 20K validation images from the ImageNet12 dataset.
We present results showing the performance of the approximations described in Section 3 in terms
of prediction accuracy, speedup gains and reduction in memory overhead. All of our ﬁne-tuning
results were achieved by training with less than 2 passes using the ImageNet12 training dataset.
Unless stated otherwise, classiﬁcation numbers refer to those of ﬁne-tuned models.

4.1 Speedup

The majority of forward propagation time is spent on the ﬁrst two convolutional layers (see Supple-
mentary Material for breakdown of time across all layers). Because of this, we restrict our attention
to the ﬁrst and second convolutional layers in our speedup experiments. However, our approxima-
tions could easily applied to convolutions in upper layers as well.
We implemented several CPU and GPU approximation routines in an effort to achieve empirical
speedups. Both the baseline and approximation CPU code is implemented in C++ using Eigen3
library [10] compiled with Intel MKL. We also use Intel’s implementation of openmp and multi-
threading. The baseline gives comparable performance to highly optimized MATLAB convolution
routines and all of our CPU speedup results are computed relative to this. We used Alex Krizhevsky’s
CUDA convolution routines 1 as a baseline for GPU comparisons. The approximation versions are
written in CUDA. All GPU code was run on a standard nVidia Titan card.
We have found that in practice it is often difﬁcult to achieve speedups close to the theoretical gains
based on the number of arithmetic operations (see Supplementary Material for discussion of the-
oretical gains). Moreover, different computer architectures and CNN architectures afford different
optimization strategies making most implementations highly speciﬁc. However, regardless of im-
plementation details, all of the approximations we present reduce both the number of operations and
number of weights required to compute the output by at least a factor of two, often more.

4.1.1 First Layer

The ﬁrst convolutional layer has 3 input channels, 96 output channels and 7x7 ﬁlters. We approx-
imated the weights in this layer using the monochromatic approximation described in Section 3.3.
The monochromatic approximation works well if the color components span a small number of one
dimensional subspaces. Figure 4.1.1 illustrates the effect of the monochromatic approximation on
the ﬁrst layer ﬁlters.
The only parameter in the approximation is C (cid:48) , the number of color channels used for the interme-
diate representation. As expected, the network performance begins to degrade as C (cid:48) decreases. The

1https://code.google.com/p/cuda-convnet/

6

Figure 3: Empirical speedups on (Left) CPU and (Right) GPU for the ﬁrst layer. C (cid:48) is the number
of colors used in the approximation.

Figure 4: Empirical speedups for second convolutional layer. (Left) Speedups on CPU using biclus-
tered (G = 2 and H = 2) with SVD approximation. (Right) peedups on GPU using biclustered
(G = 48 and h = 2) with outer product decomposition approximation.

number of ﬂoating point operations required to compute the output of the monochromatic convolu-
tion is reduced by a factor of 2 − 3×, with the larger gain resulting for small C (cid:48) . Figure 3 shows the
empirical speedups we achieved on CPU and GPU and the corresponding network performance for
various numbers of colors used in the monochromatic approximation. Our CPU and GPU imple-
mentations achieve empirical speedups of 2 − 2.5× relative to the baseline with less than 1% drop
in classiﬁcation performance.

4.1.2 Second Layer

The second convolutional layer has 96 input channels, 256 output channels and 5x5 ﬁlters. We
approximated the weights using the techniques described in Section 3.4. We explored various con-
ﬁgurations of the approximations by varying the number of input clusters G, the number of output
clusters H and the rank of the approximation (denoted by K1 and K2 for the SVD decomposition
and K for the outer product decomposition).
Figure 4 shows our empirical speedups on CPU and GPU and the corresponding network perfor-
mance for various approximation conﬁgurations. For the CPU implementation we used the bi-
clustering with SVD approximation. For the GPU implementation we using the biclustering with
outer product decomposition approximation. We achieved promising results and present speedups
of 2 − 2.5× relative to the baseline with less than a 1% drop in performance.

4.2 Combining approximations

The approximations can also be cascaded to provide greater speedups. The procedure is as fol-
lows. Compress the ﬁrst convolutional layer weights and then ﬁne-tune all the layers above until
performance is restored. Next, compress the second convolutional layer weights that result from

7

Approximation method

Number of parameters

Approximation
hyperparameters

Reduction
in weights

Increase
in error

Standard colvolution
Conv layer 1: Monochromatic
Conv layer 2: Biclustering
+ outer product decomposition
Conv layer 2: Biclustering + SVD
Standard FC
FC layer 1: Matrix SVD

FC layer 2: Matrix SVD

FC layer 3: Matrix SVD

CX Y F
CC (cid:48) + X Y F
GHK ( C
G + X Y + F
H )

GH ( C
G K1 + K1X Y K2 + K2
NM
N K + KM

F

H )

N K + KM

N K + KM

C (cid:48) = 6
G = 48; H = 2; K = 6

G = 2; H = 2; K1 = 19; K2 = 24

K = 250
K = 950
K = 350
K = 650
K = 250
K = 850

3×

5.3×

3.9×
13.4×
3.5×
5.8×
3.14×
8.1×
2.4×

0.43%
0.68%

0.9%

0.8394%
0.09%
0.19%
0.06%
0.67%
0.02%

Table 2: Number of parameters expressed as a function of hyperparameters for various approxima-
tion methods and empirical reduction in parameters with corresponding network performance.

the ﬁne-tuning. Fine-tune all the layers above until performance is restored and then continue the
process.
We applied this procedure to the ﬁrst two convolutional layers. Using the monochromatic approxi-
mation with 6 colors for the ﬁrst layer and the biclustering with outer product decomposition approx-
imation for the second layer (G = 48; H = 2; K = 8) and ﬁne-tuning with a single pass through
the training set we are able to keep accuracy within 1% of the original model. This procedure could
be applied to each convolutional layer, in this sequential manner, to achieve overall speedups much
greater than any individual layer can provide. A more comprehensive summary of these results can
be found in the Supplementary Material.

4.3 Reduction in memory overhead

In many commercial applications memory conservation and storage are a central concern. This
mainly applies to embedded systems (e.g. smartphones), where available memory is limited, and
users are reluctant to download large ﬁles. In these cases, being able to compress the neural network
is crucial for the viability of the product.
In addition to requiring fewer operations, our approximations require signiﬁcantly fewer parame-
ters when compared to the original model. Since the majority of parameters come from the fully
connected layers, we include these layers in our analysis of memory overhead. We compress the
fully connected layers using standard SVD as described in 3.2.2, using K to denote the rank of the
approximation.
Table 2 shows the number of parameters for various approximation methods as a function of hy-
perparameters for the approximation techniques. The table also shows the empirical reduction of
parameters and the corresponding network performance for speciﬁc instantiations of the approxima-
tion parameters.

5 Discussion

In this paper we have presented techniques that can speed up the bottleneck convolution operations
in the ﬁrst layers of a CNN by a factor 2 − 3×, with negligible loss of performance. We also show
that our methods reduce the memory footprint of weights in the ﬁrst two layers by factor of 2 − 3×
and the fully connected layers by a factor of 5 − 13×. Since the vast majority of weights reside in
the fully connected layers, compressing only these layers translate into a signiﬁcant savings, which
would facilitate mobile deployment of convolutional networks. These techniques are orthogonal to
other approaches for efﬁcient evaluation, such as quantization or working in the Fourier domain.
Hence, they can potentially be used together to obtain further gains.
An interesting avenue of research to explore in further work is the ability of these techniques to
aid in regularization either during or post training. The low-rank projections effectively decrease
number of learnable parameters, suggesting that they might improve generalization ability. The
regularization potential of the low-rank approximations is further motivated by two observations.
The ﬁrst is that the approximated ﬁlters for the ﬁrst conolutional layer appear to be cleaned up
versions of the original ﬁlters. Additionally, we noticed that we sporadically achieve better test error
with some of the more conservative approximations.

8

References

[1] Sermanet, P., Eigen, D., Zhang, X., Mathieu, M., Fergus, R., LeCun, Y.: Overfeat: Inte-
grated recognition, localization and detection using convolutional networks. arXiv preprint
arXiv:1312.6229 (2013)
[2] Denil, M., Shakibi, B., Dinh, L., Ranzato, M., de Freitas, N.: Predicting parameters in deep
learning. arXiv preprint arXiv:1306.0543 (2013)
[3] Hinton, G.E., Srivastava, N., Krizhevsky, A., Sutskever, I., Salakhutdinov, R.R.:
Im-
proving neural networks by preventing co-adaptation of feature detectors.
arXiv preprint
arXiv:1207.0580 (2012)
[4] Vanhoucke, V., Senior, A., Mao, M.Z.: Improving the speed of neural networks on cpus. In:
Proc. Deep Learning and Unsupervised Feature Learning NIPS Workshop. (2011)
[5] Mathieu, M., Henaff, M., LeCun, Y.: Fast training of convolutional networks through ffts.
arXiv preprint arXiv:1312.5851 (2013)
[6] Jaderberg, M., Vedaldi, Andrea, Zisserman, A.: Speeding up convolutional neural networks
with low rank expansions. arXiv preprint arXiv:1405.3866 (2014)
[7] Zhang, T., Golub, G.H.: Rank-one approximation to high order tensors. SIAM J. Matrix Anal.
Appl. 23(2) (February 2001) 534–550
[8] Zeiler, M.D., Fergus, R.: Visualizing and understanding convolutional neural networks. arXiv
preprint arXiv:1311.2901 (2013)
[9] Deng, J., Dong, W., Socher, R., Li, L.J., Li, K., Fei-Fei, L.: ImageNet: A Large-Scale Hierar-
chical Image Database. In: CVPR09. (2009)
[10] Guennebaud, G., Jacob, B., et al.: Eigen v3. http://eigen.tuxfamily.org (2010)
[11] Zeiler, M.D., Taylor, G.W., Fergus, R.: Adaptive deconvolutional networks for mid and high
level feature learning. In: Computer Vision (ICCV), 2011 IEEE International Conference on,
IEEE (2011) 2018–2025
[12] Le, Q.V., Ngiam, J., Chen, Z., Chia, D., Koh, P.W., Ng, A.Y.: Tiled convolutional neural
networks. In: Advances in Neural Information Processing Systems. (2010)
[13] Le, Q.V., Ranzato, M., Monga, R., Devin, M., Chen, K., Corrado, G.S., Dean, J., Ng,
A.Y.: Building high-level features using large scale unsupervised learning. arXiv preprint
arXiv:1112.6209 (2011)
[14] Lowe, D.G.: Object recognition from local scale-invariant features.
In: Computer vision,
1999. The proceedings of the seventh IEEE international conference on. Volume 2., Ieee (1999)
1150–1157
[15] Krizhevsky, A., Sutskever, I., Hinton, G.:
Imagenet classiﬁcation with deep convolutional
neural networks. In: Advances in Neural Information Processing Systems 25. (2012) 1106–
1114

9

Supplement to
“Exploiting Linear Structure Within Convolutional Networks
for Efﬁcient Evaluation” NIPS2014

A Forward propagation time breakdown

Table 3 shows the time breakdown of forward propagation for each layer in the CNN architecture
we explored. Close to 90% of the time is spent on convolutional layers, and within these layers the
majority of time is spent on the ﬁrst two.

Layer Time per batch (sec)

Layer Time per batch (sec)

2.8317 ± 0.1030
0.1059 ± 0.0154
0.1918 ± 0.0162
4.2626 ± 0.0740
0.0705 ± 0.0029
0.0772 ± 0.0027
1.8689 ± 0.0577
0.0532 ± 0.0018
1.5261 ± 0.0386
1.4222 ± 0.0416
0.0102 ± 0.0006
0.3777 ± 0.0233
0.0709 ± 0.0038
0.0168 ± 0.0018
0.0028 ± 0.0015
12.8885

Fraction

21.97%
0.82%
1.49%
33.07%
0.55%
0.60%
14.50%
0.41%
11.84%
11.03%
0.08%
2.93%
0.55%
0.13%
0.02%

Conv1
MaxPool
LRNormal
Conv2
MaxPool
LRNormal
Conv3
MaxPool
Conv4
Conv5
MaxPool
FC
FC
FC
Softmax
Total

Conv1
MaxPool
LRNormal
Conv2
MaxPool
LRNormal
Conv3
MaxPool
Conv4
Conv5
MaxPool
FC
FC
FC
Softmax
Total

0.0604 ± 0.0112
0.0072 ± 0.0040
0.0041 ± 0.0043
0.4663 ± 0.0072
0.0032 ± 0.0000
0.0015 ± 0.0003
0.2219 ± 0.0014
0.0016 ± 0.0000
0.1991 ± 0.0001
0.1958 ± 0.0002
0.0005 ± 0.0001
0.0077 ± 0.0013
0.0017 ± 0.0001
0.0007 ± 0.0002
0.0038 ± 0.0098

1.1752

Fraction

5.14%
0.61%
0.35%
39.68%
0.27%
0.13%
18.88%
0.14%
16.94%
16.66%
0.04%
0.66%
0.14%
0.06%
0.32%

Table 3: Evaluation time in seconds per layer on CPU (left) and GPU (right) with batch size of 128.
Results are averaged over 8 runs.

B Theoretical speedups

We can measure the theoretically achievable speedups for a particular approximation in term of the
number of ﬂoating point operations required to compute the target output. While it is unlikely that
any implementation would achieve speedups equal to the theoretically optimal level, the number of
necessary ﬂoating point operations still provides an informative upper bound on the gains.
Table 4 shows the theoretical speedup of the monochromatic approximation. The majority of the
operations result from the convolution part of the computation. In comparison, the number of oper-
ations required for the color transformation is negligible. Thus, the theoretically achievable speedup
decreases only slightly as the number of color components used is increased.
Figure 5 plots the theoretically achievable speedups against the drop in classiﬁcation performance
for various conﬁgurations of the biclustering with outer product decomposition technique. For a
given setting of input and output clusters numbers, the performance tends to degrade as the rank is
decreased.

C Combined results

We used the monochromatic approximation with 6 colors for the ﬁrst layer. Table 5 summarizes the
results after ﬁne-tuning for 1 pass through the ImageNet12 training data using a variety of second
layer approximations.

10

Number of colors

4
6
8
12
16
24

Original

24.1%
16.1%
9.9%
3.5%
1.99%
1.43%

Increase in test error
(cid:107)W (cid:107)data distance metric

5.9%
2.4%
1.4%
0.7%
0.8%
0.4%

Theoretical speedup

Fine-tuned

1.9%
0.4%
0.2%
0%
-
-

2.97×
2.95×
2.94×
2.91×
2.88×
2.82×

Table 4: Performance when ﬁrst layer weights are replaced with monochromatic approximation and
the corresponding theoretical speedup. Classiﬁcation error on ImageNet12 validation images tends
to increase as the approximation becomes harsher (i.e. fewer colors are used). Theoretical speedups
vary only slightly as the number of colors used increases since the color transformation contributes
relatively little to the total number of operations.

Figure 5: Theoretically achievable speedups vs. classiﬁcation error for various biclustering approx-
imations.

Method

Layer 2

Biclustering
+ outer product decomposition
Biclustering
+ outer product decomposition
Biclustering + SVD
Biclustering + SVD

Hyperparameters

G = 48; H = 2; K = 8

G = 48; H = 2; K = 6

G = 2; H = 2; K1 = 19; K2 = 64
G = 2; H = 2; K1 = 19; K2 = 51

Table 5: Cascading approximations.

11

Increase in error

1%

1.5%

1.2%
1.4%

