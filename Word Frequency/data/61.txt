P S Y C H O L O G I C A L S C I E N C E

Research Report

Becoming a Face Expert

Catherine J. Mondloch,1 Daphne Maurer,2 and Sara Ahola2

1Brock University, St. Catharines, Ontario, Canada, and 2McMaster University, Hamilton, Ontario, Canada

ABSTRACT—Expertise in recognizing facial identity, and, in
particular, sensitivity to subtle differences in the spacing
among facial features, improves into adolescence. To as-
sess the inﬂuence of experience, we tested adults and 8-
year-olds with faces differing only in the spacing of facial
features. Stimuli were human adult, human 8-year-old,
and monkey faces. We show that adults’ expertise is shaped
by experience: They were 9% more accurate in seeing
differences in the spacing of features in upright human
faces than in upright monkey faces. Eight-year-olds were
14% less accurate than adults for both human and monkey
faces (Experiment 1), and their accuracy for human faces
was not higher for children’s faces than for adults’ faces
(Experiment 2). The results indicate that improvements in
face recognition after age 8 are not related to experience
with human faces and may be related to general im-
provements in memory or in perception (e.g., hyperacuity
and spatial integration).

Adults’ expertise in face recognition is mediated by specialized
cortical mechanisms (Haxby et al., 2001; Rossion et al., 2000)
and facilitated by exquisite sensitivity to subtle differences
among individual faces in the spacing of facial features (second-
order relations) (Freire, Lee, & Symons, 2000; Kemp, McManus,
& Pigott, 1990; Mondloch, Le Grand, & Maurer, 2002; Rhodes,
Brake, & Atkinson, 1993; Rhodes, Carey, Byatt, & Profﬁtt,
1998; Searcy & Bartlett, 1996), both of which are slow to de-
velop. The N170, an event-related potential evoked selectively
by faces, has a smaller amplitude and a longer latency in chil-
dren than in adults, even in midadolescence (Taylor, McCarthy,
Saliba, & Degiovanni, 1999). In functional magnetic resonance
imaging studies, the fusiform face area does not respond more to
faces than to other categories until children are around 10 years
of age (Aylward et al., 2005; Gathers, Bhatt, Corbly, Farley, &
Joseph, 2004), and even in 12- to 14-year-olds, it is not as se-
lectively activated by faces as it is in adults (Aylward et al.,

Address correspondence to Cathy Mondloch, Department of Psy-
chology, Brock University, 500 Glenridge Ave., St. Catharines,
Ontario, Canada L2S 3A1, e-mail: cmondloch@brocku.ca.

2005). Behavioral results also show differences between chil-
dren and adults. When asked to make same/different judgments
about pairs of faces that differ only in the spacing of facial
features, 6-year-olds perform at above-chance levels, but even
14-year-olds make more errors than adults (Mondloch et al.,
2002; Mondloch, Le Grand, & Maurer, 2003; see also Freire &
Lee, 2001).
One hypothesis is that sensitivity to second-order relations
improves as children acquire additional experience differenti-
ating individual faces (Nelson, 2003). Diamond and Carey
(1986) argued that children under 10 years of age are like adult
novices attempting to recognize individual dogs: Both lack ex-
pertise in using second-order relations to differentiate among
individuals. Alternatively, children’s poor performance may
reﬂect general cognitive limitations (see Gilchrist & McKone,
2003, for a discussion of this hypothesis and Pellicano, Rhodes,
& Peters, 2006, for evidence of early skill in using second-order
relations under other conditions).
We took a novel approach to investigating the development of
expertise in recognizing facial identity. Adults’ expertise is
limited to stimuli with which they have a wealth of experience—
typically upright human faces of their own race. They make more
errors when human faces are inverted (Collishaw & Hole, 2000;
Yin, 1969) and when faces are of an unfamiliar race (Rhodes,
Hayward, & Winkler, 2006; Sangrigoli, Pallier, Argenti, Ven-
tureyra, & de Schonen, 2005). Also, the amplitude of the N170
is larger for human than for monkey faces (de Haan, Pascalis,
& Johnson, 2002), and after becoming familiar with a single
human face, adults look longer at a novel human face, but
after becoming familiar with a single monkey face, they do not
look longer at a novel monkey face (Pascalis, de Haan, &
Nelson, 2002).
To determine whether the pattern of adultlike expertise
emerges by 8 years of age, we asked adults and 8-year-olds to
make same/different judgments for two sets of faces—human
faces and monkey faces—that differed only in the spacing
among features. Between age 8 and adulthood, children have a
wealth of experience differentiating individual upright human
faces, but except for rare cases (e.g., curators at zoos), no adult or
child has experience recognizing individual monkey faces. If the
large improvement in sensitivity to the spacing of features in

930

Copyright r 2006 Association for Psychological Science

Volume 17—Number 11

Catherine J. Mondloch, Daphne Maurer, and Sara Ahola

upright human faces that occurs between 8 and 141 years of age
depends on years of experience differentiating individual hu-
man faces, then children’s performance and adults’ performance
should differ more for human faces than for monkey faces be-
cause additional experience discriminating monkey faces is not
acquired during adolescence. If the difference between chil-
dren’s and adults’ performance is identical for monkey and
human faces, then the improvement exhibited by adults must be
related to more general visual or cognitive development. The
data from adults also provide an opportunity to compare directly
their sensitivity to spacing in human versus nonhuman faces. We
presented faces both upright and inverted. Inversion disrupts
adults’ sensitivity to the spacing among facial features more than
their sensitivity to other cues to facial identity (Freire et al.,
2000; Leder & Bruce, 2000; Mondloch et al., 2002; Rhodes
et al., 2006) and provides another measure of expertise.

EXPERIMENT 1

Method

Subjects included twenty-four 8-year-olds ( 3 months; M 5
Subjects
8.06; range 5 7.8 to 8.24) and 24 adults (ages 18 to 28 years).
All were right-handed, were Caucasian, and had normal vision
(for details of the screening tests, see Mondloch et al., 2002). An
additional 26 children were excluded from the ﬁnal analysis
because they failed visual screening (n 5 13), the practice task
(n 5 4), or the cousins trials (see Procedure; n 5 6) or were
uncooperative (n 5 3). An additional 9 adults were excluded
from the ﬁnal analysis because they failed visual screening (n 5
8) or the handedness test (n 5 1).

Stimuli
We created two sets of faces that differed only in the spacing
among facial features; each set consisted of one original face (an
adult female or a monkey) and four manipulated versions of that
face (referred to as sisters) in which the eyes and mouth were
moved (see Figs. 1a and 1b). We equated the height of the human
faces (the distance between the surgical cap and the chin) with
the height of the monkey faces (the distance between the fur on
the forehead and the chin). The spacing changes were identical
across the two face sets, and the same as in our previous studies
with human faces (Mondloch et al., 2002). We also created two
face sets that consisted of the original human or monkey face
plus three completely different human or monkey faces, re-
spectively (referred to as cousins).

Procedure
The protocol was approved by the Research Ethics Boards at
McMaster University and Brock University, Canada. Informed
consent was obtained from each adult participant and the parent
of each child; verbal assent was obtained from each child.

Fig. 1. Examples of faces used in the experiments. Adult human faces (a)
were used in both experiments; monkey faces (b) were used in Experiment
1, and 8-year-old human faces (c) were used in Experiment 2. In each set,
face variations were created by moving the eyes 4 mm (0.231 from the
testing distance of 100 cm) up, down, closer together, or farther apart,
relative to the original face, and by moving the mouth 2 mm (0.121 from 100
cm) up or down. According to anthropomorphic norms for humans
(Farkas, 1981), we moved the eyes up or down by 0.95 SD, the mouth up or
down by 1.06 SD, and the eyes closer together or farther apart by 2.60 SD
(see Mondloch, Le Grand, & Maurer, 2002, for additional details). Shown
are the original face (left) and one of the sisters (right) from each condition.

The procedure began with a practice task requiring same/
different judgments of two identical faces or two radically dif-
ferent versions of the same face (e.g., a face with eyes rotated
451 clockwise; see Mondloch et al., 2002, for details). To par-
ticipate in the main experiment, participants were required ﬁrst
to be correct on at least 10 of 12 practice trials.
On each trial of the main task, one of ﬁve possible faces ap-
peared for 200 ms, followed by a random-noise mask (300 ms)
and then by a second face that appeared until the subject re-
sponded ‘‘same’’ or ‘‘different.’’ Trials were blocked by species;
half of the participants were tested with human faces ﬁrst.
Within each species, there were three blocks of trials (for a total
of 92 trials) presented in the same order to all participants:
upright (n 5 30), inverted (n 5 30), cousins (n 5 32). Four
additional practice trials were given prior to each block. Within
each block, the correct response was ‘‘same’’ for half of the trials,

Volume 17—Number 11

931

Monkey Faces

each face served as a test face as often as the model face, and
half of the trials with each face were same trials and half were
different trials. The cousins were included to make some of the
trials easier and to provide an opportunity to identify children
who did not understand the task or were uncooperative. Par-
ticipants were included in the study only if they were correct on
at least 70% of the human cousins trials; they were not required
to be accurate on at
least 70% of
the monkey cousins
trials because these trials might be difﬁcult for all participants
(Pascalis et al., 2002).

p

p

prep

Results and Discussion
Accuracy was high for both monkey cousins (adults: M 5 .88 
.02; 8-year-olds: M 5 .76  .03) and human cousins (adults:
M 5 .90  .02; 8-year-olds: M 5 .86  .02). Because the pri-
mary question concerned accuracy with upright faces, we ana-
lyzed accuracy on upright trials using a 2 (age)  2 (species)
analysis of variance. There were main effects of age, F(1, 46) 5
2 ¼ :267, and species, F(1, 46) 5 18.37,
16.73, prep 5 .98, Z
5 .99, Z
2 ¼ :285, but there was no signiﬁcant interaction.
As shown in Figure 2a, adults were 14% more accurate than 8-
year-olds for both human and monkey faces, and both age groups
were more accurate discriminating human faces than monkey
faces, despite the fact that the magnitude of the spacing changes
was exactly the same in the two sets. The size of this species
effect was identical for adults and 8-year-olds: 9% greater ac-
curacy for human than monkey faces, a result indicating that
adults’ exquisite sensitivity to the spacing among features is
restricted to human faces and that this pattern of expertise is
already present by the time children are 8 years old. Analyses of
reaction times indicated that the pattern of results could not be
explained by a speed-accuracy trade-off.
That adults’ expertise is restricted to upright human faces is
further demonstrated by the ﬁnding that among adults, the in-
version cost was larger for human faces (M 5 .132  .028) than
for monkey faces (M 5 .028  .023), t(23) 5 2.62, prep 5 .96,
Z2 5 .13. The inversion cost for 8-year-olds is not interpretable
because their accuracy on upright monkey trials was close to a
chance value of .50.
The ﬁndings indicate that the adult pattern of expertise
emerges by the time children are 8 years old: Like adults,
children are more sensitive to second-order relations in human
faces than in monkey faces. This result is consistent with evi-
dence that perceptual narrowing begins during infancy (de Haan
et al., 2002; Halit, de Haan, & Johnson, 2003; Pascalis et al.,
2002). The results imply that after age 8, improvements on tasks
that tap sensitivity to second-order relations in faces may not
reﬂect development of face expertise from additional experi-
ence, but rather may reﬂect more general improvements in
cognition (see General Discussion).
To test this hypothesis further, in Experiment 2, we tested
a new group of adults and 8-year-olds with the adult faces used

0

Fig. 2. Mean accuracy of adults and 8-year-olds when stimuli were pre-
sented upright and inverted in Experiments 1 (a) and 2 (b). Analyses using
d
yielded the same results. Asterisks indicate that accuracy was signiﬁ-
cantly better than chance (p < .01, one-tailed) by d
analysis, with a
Bonferroni correction for the number of comparisons (adjusted a 5 .012).
Error bars indicate 1 SEM; because the analyses emphasize within-sub-
jects comparisons, the standard error bars represent intrasubject vari-
ability between conditions.

0

in Experiment 1 and a set of faces based on the face of
an 8-year-old girl. Children may have more experience differ-
entiating among children’s faces than among adults’ faces:
Whereas they typically have a few teachers and coaches, they
have dozens of classmates and teammates. If experience in
differentiating individual faces does contribute to improved
sensitivity to second-order relations in faces during childhood,
then the difference in accuracy between adults and 8-year-old
children might be larger for adults’ faces than for children’s
faces because of the greater difference between the two age
groups in the amount of experience they have differentiating
adult faces.

932

Volume 17—Number 11

Catherine J. Mondloch, Daphne Maurer, and Sara Ahola

EXPERIMENT 2

Method
Twenty-four adults (ages 18 to 28 years) and twenty-four 8-year-
old children (M 5 8.9 years; range 5 7.94 to 8.24) participated
in this experiment. All were right-handed, were Caucasian, and
had normal vision. An additional 11 children and 8 adults were
excluded from the analysis because they failed either the
practice task (2 children) or visual screening (9 children, 8
adults). We presented the same set of adult human faces as in
Experiment 1, but replaced each set of monkey faces with a set
of 8-year-old human faces (see Fig. 1c). The spacing changes
made to create the 8-year-old sisters were identical to the spac-
ing changes made to create the adult sisters and the monkey
sisters in Experiment 1. All participants met the inclusion cri-
terion of achieving 70% accuracy in responding to both the adult
and the child cousins.

p

Results and Discussion
Accuracy was high for both adult human cousins (adults: M 5
.94  .01; 8-year-olds: M 5 .90  .02) and 8-year-old cousins
(adults: M 5 .93  .02; 8-year-olds: M 5 .84  .02). Again our
primary interest was in performance on upright trials. The 2 (age
of participant)  2 (age of face) analysis of variance revealed
only a main effect of age of participant, F(1, 46) 5 7.07, prep 5
2 ¼ :133; the main effect of age of face and the inter-
.97, Z
action were both nonsignificant. As shown in Figure 2b, adults
were more accurate than 8-year-olds, and the magnitude of this
difference did not vary across face sets; 8-year-olds’ accuracy
did not vary across the two face sets. Furthermore, the analysis of
inversion cost did not reveal any signiﬁcant effects: The inver-
sion cost was not signiﬁcantly different for adult and child faces
nor for the two age groups (8-year-olds for child faces: M 5 .165
 .026; 8-year-olds for adult faces: M 5 .131  .026; adults for
child faces: M 5 .098  .028; adults for adult faces: M 5 .125 
.023). Analyses of reaction times indicated that the pattern of
results could not be explained by a speed-accuracy trade-off.
The results show that the additional experience 8-year-olds
have recognizing the identity of peers does not make their per-
formance more adultlike for children’s faces than for adults’
faces. Despite their experience discriminating schoolmates and
other children on a daily basis, 8-year-olds were less accurate
than adults in discriminating spacing differences in both chil-
dren’s and adults’ faces, and the magnitude of the performance
difference between 8-year-olds and adults was roughly the same
for the two kinds of faces (see Fig. 2b).

second-order relations in monkey faces may underlie the other-
species effect (i.e., their poorer recognition of the identity of
monkey faces compared with human faces; Pascalis et al., 2002;
our Experiment 1). Although the spacing changes were identical
across face sets in Experiment 1, adults were more accurate
when making same/different judgments about pairs of upright
human faces than when making those judgments about monkey
faces.
Although 8-year-olds were less accurate than adults when
making same/different judgments based on second-order rela-
tions in all conditions, they showed the same pattern of expertise
as adults. Like adults, they were 9% less accurate for monkey
faces than for human faces, and they were 14% less accurate
than adults for both upright human and upright monkey faces.
Furthermore, their sensitivity to second-order relations was the
same for 8-year-old faces as for adult faces; that is, their sen-
sitivity did not increase when they were tested with a set of faces
from a class they likely had more experience differentiating.
Adults were more accurate than 8-year-olds when discrim-
inating monkey faces despite having minimal exposure to this
class of stimuli. This result indicates that adults’ better per-
formance on tasks that tap sensitivity to second-order relations
in faces cannot be explained by their greater experience dif-
ferentiating individual faces, as has been postulated previously
(e.g., Nelson, 2003). Rather, the difference between adults’ and
children’s performance must be related to more general aspects
of cognitive or perceptual development. Although memory im-
proves during later childhood (e.g., Lange-Ku¨ ttner & Friederici,
2000), 8-year-olds make more errors than adults even when
faces are presented simultaneously (Mondloch, Dobson, Par-
sons, & Maurer, 2004). Two visual skills are known to improve
until age 14: Vernier acuity, a hyperacuity involving sensitivity
to slight misalignments between stimuli such as abutting lines
(Skoczenski & Norcia, 2002), and the ability to link small ori-
ented elements into a ﬂowing contour (Kovacs, Kozma, Feher, &
Benedek, 1999). Immaturities in either of these visual skills
could limit sensitivity to second-order facial relations during
development.

Acknowledgments—This research was funded by a Natural
Sciences and Engineering Research Council of Canada
(NSERC) Discovery Grant to D. Maurer and by a Research
Capacity and Development Grant (NSERC) to C. Mondloch.
Olivier Pascalis contributed the original monkey picture.
Kimberly Costello tested the adult participants in Experiment 1
as part of an independent study.

GENERAL DISCUSSION

REFERENCES

It is well known that adults’ expertise in face recognition does
not extend to other-species faces (Pascalis et al., 2002). We have
shown that adults are especially sensitive to second-order re-
lations in upright human faces; their reduced sensitivity to

Aylward, E.H., Park, J.E., Field, K.M., Parsons, A.C., Richards, T.L.,
Cramer, S.C., & Meltzoff, A.N. (2005). Brain activation during face
perception: Evidence of a developmental change. Journal of
Cognitive Neuroscience, 17, 308–319.

Volume 17—Number 11

933

Monkey Faces

Collishaw, S.M., & Hole, G.J. (2000). Featural and conﬁgural processes
in the recognition of faces of different familiarity. Perception, 29,
893–909.
de Haan, M., Pascalis, O., & Johnson, M.H. (2002). Specialization of
neural mechanisms underlying face recognition in human infants.
Journal of Cognitive Neuroscience, 14, 199–209.
Diamond, R., & Carey, S. (1986). Why faces are and are not special: An
effect of expertise. Journal of Experimental Psychology: General,
115, 107–117.
Farkas, L.G. (1981). Anthropometry of the head and face in medicine.
New York: Elsevier.
Freire, A., & Lee, K. (2001). Face recognition in 4- to 7-year-olds:
Processing of conﬁgural, featural, and paraphernalia information.
Journal of Experimental Child Psychology, 80, 347–371.
Freire, A., Lee, K., & Symons, L.A. (2000). The face-inversion effect as
a deficit in the encoding of conﬁgural information: Direct evi-
dence. Perception, 29, 159–170.
Gathers, A.D., Bhatt, R., Corbly, C.R., Farley, A.B., & Joseph, J.E.
(2004). Developmental shifts in cortical loci for face and object
recognition. NeuroReport, 15, 1549–1553.
Gilchrist, A., & McKone, E. (2003). Early maturity of face processing in
children: Local and relational distinctiveness effects in 7-year-
olds. Visual Cognition, 10, 769–793.
Halit, H., de Haan, M., & Johnson, M.H. (2003). Cortical speciali-
zation for face processing: Face-sensitive event-related potential
components in 3- and 12-month-old infants. NeuroImage, 19,
1180–1193.
Haxby, J.V., Gobbini, M.I., Furey, M.L., Ishai, A., Schouten, J.L., &
Pietrini, P. (2001). Distributed and overlapping representations of
faces and objects in ventral temporal cortex. Science, 293, 2425–
2430.
Kemp, R., McManus, C., & Pigott, T. (1990). Sensitivity to the dis-
placement of facial features in negative and inverted images.
Perception, 19, 531–543.
Kovacs, P., Kozma, P., Feher, A., & Benedek, G. (1999). Late maturation
of visual spatial
integration in humans. Proceedings of
the
National Academy of Sciences, USA, 96, 12204–12209.
Lange-Ku¨ ttner, C., & Friederici, A.D. (2000). Modularity of object and
place memory in children. Brain and Cognition, 43, 302–305.
Leder, H., & Bruce, V. (2000). When inverted faces are recognized: The
role of conﬁgural information in face recognition. The Quarterly
Journal of Experimental Psychology, 53, 513–536.
Mondloch, C.J., Dobson, K.S., Parsons, J., & Maurer, D. (2004). Why 8-
year-olds can’t tell the difference between Steve Martin and Paul
Newman: Factors contributing to the slow development of sensi-
tivity to the spacing of facial features. Journal of Experimental
Child Psychology, 89, 159–181.
Mondloch, C.J., Le Grand, R., & Maurer, D. (2002). Conﬁgural face
processing develops more slowly than featural face processing.
Perception, 31, 553–566.

Mondloch, C.J., Le Grand, R., & Maurer, D. (2003). Early visual ex-
perience is necessary for the development of some—but not all—
aspects of face processing. In O. Pascalis & A. Slater (Eds.),
The development of face processing in infancy and early childhood
(pp. 99–117). New York: Nova.
Nelson, C.A. (2003). The development of face recognition reﬂects an
experience-expectant and activity-dependent process. In O.
Pascalis & A. Slater (Eds.), The development of face processing in
infancy and early childhood (pp. 79–97). New York: Nova.
Pascalis, O., de Haan, M., & Nelson, C.A. (2002). Is face processing
species specific during the ﬁrst year of
life? Science, 296,
1321–1323.
Pellicano, E., Rhodes, G., & Peters, M. (2006). Are preschoolers sen-
sitive to conﬁgural information in faces? Developmental Science,
9, 270–277.
Rhodes, G., Brake, S., & Atkinson, A.P. (1993). What’s lost in inverted
faces? Cognition, 47, 25–57.
Rhodes, G., Carey, S., Byatt, G., & Profﬁtt, F. (1998). Coding spatial
variations in faces and simple shapes: A test of two models. Vision
Research, 38, 2307–2321.
Rhodes, G., Hayward, W.G., & Winkler, C. (2006). Expert face coding:
Conﬁgural and component coding of own-race and other-race
faces. Psychonomic Bulletin & Review, 13, 499–505.
Rossion, B., Gauthier, I., Tarr, M.J., Despland, P., Bruyer, R., Linotte,
S., & Crommelinck, M. (2000). The N170 occipito-temporal
component is delayed and enhanced to inverted faces but not to
inverted objects: An electrophysiological account of face-specific
processes in the human brain. NeuroReport, 11, 69–74.
Sangrigoli, S., Pallier, C., Argenti, A.M., Ventureyra, V.A.G., & de
Schonen, S. (2005). Reversibility of the other-race effect in
face recognition during childhood. Psychological Science, 16,
440–444.
Searcy, J.H., & Bartlett, J.C. (1996). Inversion and processing of
component and spatial-relational information in faces. Journal of
Experimental Psychology: Human Perception and Performance,
22, 904–915.
Skoczenski, A.M., & Norcia, A.M. (2002). Late maturation of visual
hyperacuity. Psychological Science, 13, 537–541.
Taylor, M.J., McCarthy, G., Saliba, E., & Degiovanni, E. (1999). ERP
evidence of developmental changes in processing of
faces.
Clinical Neurophysiology, 100, 910–915.
Yin, R.K. (1969). Looking at upside-down faces. Journal of Experi-
mental Psychology, 81, 141–145.

(RECEIVED 10/12/05; REVISION ACCEPTED 3/20/06;
FINAL MATERIALS RECEIVED 3/30/06)

934

Volume 17—Number 11

