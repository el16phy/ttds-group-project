GPU powered CNN simulator (SIMCNN) with
graphical ﬂow based programmability

Balázs Gergely Soós

Faculty of Information Technology
Pázmány Péter Catholic University
also with the Computer and Automation
Research Institute of the Hungarian
Academy of Sciences, Budapest, Hungary
Email: soos@digitus.itk.ppke.hu

Ádám Rák, József Veres

Faculty of Information Technology
Pázmány Péter Catholic University
Práter u. 50/A, H-1083
Budapest, Hungary
Email: rakad@digitus.itk.ppke.hu,
verjo@digitus.itk.ppke.hu

György Cserey

Infobionic and Neurobiological Plasticity
Research Group, Hungarian Academy of
Sciences at Pázmány University and
Semmelweis University, Budapest
Práter utca 50/a. H-1083 Hungary
Email: cserey@itk.ppke.hu

Abstract— In this paper, we introduce an innovative CNN
algorithm development environment
that signiﬁcantly assists
algorithmic design. The introduced graphical user interface uses
Matlab Simulink with UMF-like program description, where
direct functionality accompanies better accessability. The new
generation of graphical cards incorporate many general pur-
pose Graphics Processing Units, giving the power of parallel
computing to a simple PC environment cheaply. Therefore,
analysis of CNN dynamics become more feasible with a common
hardware setup. Our measurements demonstrate the efﬁciency of
the realized system. In the case of simpler algorithms, real-time
execution is also possible.

In the next section, a short explanation of our decision is
presented, including the architecture of the novel generation
of NVIDIA graphics cards. In the third section, we will
introduce SIMCNN in details. In the fourth section, we present
the hardware mapping for the dynamic equations. Finally,
experimental results are given covering a simple implemented
algorithm and performance comparison with other simulating
platforms.

I I . GPU A S A HARDWARE ACC EL ERATOR

I . IN TRODUC T ION

CNN-UM has numerous analogue and digital hardware
implementations [8], [9], [10], [11], [12], [13], [14] that are
ideal tools for processing two dimensional topological ﬂows
(especially for visual ones). They have speciﬁc development
environment for programming; however, a high-level UMF
(Universal Machine on Flows) model and the dependent UMF
charts exist for describing these algorithms.
We developed a simulator system for running these graphi-
cally represented algorithms directly. More precisely, one can
draw UMF-like diagrams with simple drag&drop technique
and test them before optimizing them to a speciﬁc hardware.
We are using SIMULINK from the MathWorks Inc. because
it is able to simulate almost any dynamic system constructed
from simple blocks. A block-set had been created contain-
ing necessary high level blocks for the CNN-UM structure
including simple access for the one-layer, linear type subset
of the template library. Using SIMULINK’s built-in solver, it
is ideal for educational purposes. If we exchange the most
computational expensive part, namely if we are running the
CNN dynamics on a faster external hardware or software
component, we get a powerful development environment that
combines the advantages of UMF modelling with affordable
simulation speed. The most common hardware accelerators
for a desktop PC are the multi-cored CPUs and the Graphical
Processing Units (GPUs). We have chosen the GPU for our
purpose. As we know another group is also working on GPU
acceleration of the template running. [17]

Fig. 1. Comparison in giga ﬂoating point operations per second between the
last few years’ computational capabilities of CPUs and GPUs, where we can
realize a huge increase in the PC’s video card areas. [5]

A. CPU vs. GPU comparison
By the end of 2007, more than a billion transistors [4]
could be found on a single digital chip. This means a huge
opportunity for creating not only single but dual, quad or
many cores on a single die. But this way of getting more
and more computational power for a desktop PC inﬂuenced
not the Central but the Graphics Processing Units ﬁrst. This
could be proved by the fact that at this time anybody can buy
a GPU with 128 processors, but only dual or quad cores are
common on nowadays CPU’s market. In Fig. 1. the rise of the
two typical processor types can be followed.
The diagram shows actually a more than ﬁve times greater
computational capability in desktop GPUs that we can achieve

compared to nowadays’ worldwide used personal computer’s
CPU. This is the result of the multiple cores driven by a
higher memory bandwidth. Furthermore, the main signiﬁcant
difference is the transistor count that is dedicated to the data
processing elements. The high percentage of the transistors in
CPUs is responsible for the ﬂow control and for the caching.
If we are not only analysing the desktop models from the
CPU side, there are many competitors that could come up.
One could be the well known Cell processor from the STI
alliance (Sony, Toshiba and IBM). This is a very powerful
digital processor but for some applications, the most advanced
NVIDIA card, GF8800GTX overperforms it. While the Cell
Broadband Engine Architecture with 8 Synergistic Processing
Elements (SPE) clocked at 3.2Ghz is capable of 208.4 Gﬂops
[6],
the 8800GTX with 128 stream processors clocked at
1.35Ghz reaches roughly 520 Gﬂops [7] for nearly the same
price.
So we can state that today’s GPUs with their high level
of parallelism are cost-efﬁcient processors for performing the
CNN simulator’s power resource extensive task, namely the
template running.

B. CUDA: NVIDIA’s newest architecture
In order to realize a non-graphic operation like template
running, on the graphics card there must be a software and
hardware solution to access the high computational power of
the GPU. This need is fulﬁlled by NVIDIA’s newest devel-
opment, the Compute Uniﬁed Device Architecture (CUDA).
This is a totally new approach where we can build our code
in C-like language without any knowledge about graphical
programming like DirectX or OpenGL. Fortunately, it is not
only available on the high-end category but also on the
whole NVIDIA’s 8 series cards. This means that it could
get very common soon, and anybody will be able to beneﬁt
from its advantages for a low price. Before the technical
implementation, we highlight the major hardware parts of the
GPUs architecture.
In Fig. 2. we can see the hardware model of this archi-
tecture. We can realize that there are no pixel and vertex
shaders we were used to on the previous series of the
video cards. Instead of that, uniﬁed, scalar-based processors
were introduced. Eight of these processors are grouped with
an Instruction Unit to form a single-instruction-multiple-data
(SIMD) multiprocessor (MP). The number of MPs are varying
from 1 to 16, which means 8 to 128 parallel processors. Global
memories are much faster than those we are using in our main
boards, but these would be slow for supplying the data for all
processors. Therefore we have three ways for caching. A 16
KB per MP high-speed universal purpose shared memory is
available beside the constant and texture caching memory.

I I I . S IMCNN AND I T S GRA PH ICA L FLOW BA S ED
PROGRAMMAB I L I TY

A. The software framework
The simulator is based on SIMULINK from The Math-
Works Inc. It is a worldwide used software that has support

Fig. 2. Hardware model of the GeForce 8 series cards. There are no separate
vertex and pixel shaders. It is a new uniﬁed hardware architecture where we
can ﬁnd multiprocessors (MPs) that have a shared memory and a few scalar-
based processors. [5]

nearly for any operating systems. Graphical ﬂow based pro-
grammability has many advantages compared to the standard
imperative command based ways. The perspicuity of the
algorithm is much higher because there are just a few well
declared blocks we have to work with. In the SIMCNN
blockset, we deﬁned necessary extensions for accessing the
video card and to run templates on it (Fig. 3). SIMULINK
has built in blocks for realizing speciﬁc data ﬂow structures,
like parallel or conditional execution and signal routing. Once
an image is uploaded to the device, we can refer to it using
a real value as ID-like pointer in C. For the ﬁrst execution
time, the code is only initializing using the surrounding block’s
IDs to determine its real dimensions. After the initialization
phase, template running will be performed. SIMULINK calls
event handler functions of each block one by one in a good
serialized order. Our blocks are implemented using the s-
function interface of SIMULINK to produce interface for the
CUDA based binaries. We also created a small user interface
for browsing the linear one-layered subset of the template
library. The user can also have custom templates.
B. UMF description
UMF stands for Universal Machines on Flows [2]. The UMF
library [3] contains the basic image processing algorithms and
numerous powerful ones like the CenterPointDetector or the
GlobalConnectivityDetection. This library can give idea for
developers for creating their own programs.
CNN template running and special ﬂow control marks are
the basic operations of the UMF.
1) With the additional blocks: The “Elementary instruc-
tion” is the basic template that is implemented by the block
that can be seen on Fig. 3. The parameters can be set using

the user interface. Template values can be supplied by external
matrices also. “Parametric templates” can be realized this way.
Some of the possible “Operators” like the Global White or
Black is also implemented with the blocks named GBW and
GBB respectively.

Fig. 3.
In this ﬁgure, the blocks for communication with the GPU and
template running are presented. They are implemented using C language S-
functions. Using a mask (Simulink GUI) for the template runner block, the
user can load an item from the template library or specify a custom operation.

2) With the built-in blocks: For creating the data path,
basic SIMULINK knowledge is needed. Fortunately MATLAB
help is quite detailed and has good tutorials for learning.
The video processing blockset gives the ability to handle
two dimensional input and output data like images and video
streams. For implementing “variables” built in Memory blocks
can be used. To realize “Triggers” Unit Delay Enabled blocks
can be applied. We can specify the starting value. The input
ﬂow is blocked if the condition is not satisﬁed. Complex ﬂow
sequences like conditional loops can be joined together using
conditions that can be realized using “multiplexer” blocks.
We can create “subsystems” using built-in mechanism to align
algorithmic parts. “Enabled subsystems” combined with If or
Switch blocks can be used for branching.

C. Template running - the approximate equation of the CNN

Let us consider the standard CNN conﬁguration with space
Apq , Bpq where p,q∈ {−r, .., r} and bias map zij .
invariant templates, connection radius r , the cloning templates
ˆAij (y) = X
ˆBij (u) = X

= −xij + ˆAij (y) + ˆBij (u) + zij

Bpq ∗ ui+p,j+q (t)

Apq ∗ yi+p,j+q (t)

p,q∈{−r,..,r}

dxij
dt

(1)

(2)

(3)

p,q∈{−r,..,r}

yij = f (xij )

(4)

As differential equations can be solved iteratively with Euler
formula, the Eq. 1 turns to be the Eq. 5.

xij (t + dt) = xij (t) + dt ∗ x

0

ij

(5)

So we can expose it (Eq. 5.) into the following equation
(Eq. 6)

xij (t + dt) = D ∗ xij (t) + Vij (x) + W

(6)

Where (Eq. 6) we have applied the Eq. 7, the Eq. 8., the
Eq. 9. and the Eq. 4.
Vij (x) = X

D = 1 − dt

(7)
(8)

p,q∈{−r,..,r}

[dt ∗ Apq ] ∗ f [xi+p,j+q (t)]
h ˆBij (u) + zij

i ∗ dt

Wij =

(9)

If the input picture does not change over the template exe-
cution time, we can precalculate W . We can also calculate and
store dt ∗ Apq . Altogether we will need two two dimensional
data set ( state matrix and W ) and the constant template values
to calculate the state update for the next iteration .

IV. MA P P ING

CUDA supports implementing parallel algorithms with
topological processing, running the same code segment on all
nodes. This is similar to the CNN architecture. Since the num-
ber of the physical computing elements are typically less then
the appropriate number of nodes, an automatic serialization
is applied. The common algorithm running on the nodes is
called kernel. All node has its own thread that is its execution
context. Blocks are groups of topologically associated threads.
This low-level cluster is especially important because all
threads in the block are executed on the same MP, so they have
a common high-speed, low-latency (1-2 clock cycle) shared
memory that can be used for creating local interconnection.
It is tempting to group all nodes to a single block in order to
form a fully connected network. All threads need space for
storing their data and the amount of memory available is only
8 Kbytes, therefore we have a hard limitation. Also this would
produce unbalanced load on the MPs.
The communication between the threads can be realized
only through memory transfers. Synchronization is needed due
to the serialization in order to preserve checkpoints to avoid
inconsistencies in communications. We have to minimize data
exchange between threads that are mapped to distinct blocks,
because the only way to do that
is accessing the high-
latency (100-200 clock cycle) global memory. Since there is no
support for the synchronization outside the blocks, the whole
kernel should be executed. This is splitting algorithms into
smaller kernels.
The actual topological problem is the CNN dynamic equa-
tion. Analysing the equation, one could see that this problem
is a memory intensive one. To calculate the state update for
a pixel, the constants (D ,Vij ), the nine state variables from
the neighbourhood and the bias map value (Wij ) is needed. If
all pixels are accessing their nine neighbours separately, that

Fig. 4. CUDA’s virtual layer for topological parallel processing. Its basic
elements are the threads that can form a block. Blocks can form a grid that
is assigned to a kernel. [5]

would not be efﬁcient. We can apply 2D aware texture cache
to assist joint prefetch, but the cache hit rate is less than 100%.
We can eliminate unnecessary redundant memory access with
direct copy of all referred states into the the shared memory.
The small amount of constant values can ﬁt into the constant
cache.
The only opportunity for the threads to produce persistent
output is to write into the global memory. The memory access
for this has high-latency, so we have to utilize the maximal
bus width. The system gives support for four element vectors
beside the standard data types. If a thread is responsible for
more pixels, the outputs can be packed using vector represen-
tations. On the other hand, simple branchless algorithms are
needed for efﬁcient parallel execution. Therefore we associate
four pixels to a thread. This deﬁnes the number of the threads
for a speciﬁc image size. Then we have to consider the optimal
conﬁguration for grouping the threads. If we form square-like
clusters, the ratio between area and perimeter is optimal so
the number of bordering nodes are minimal. We investigated
the relationship between the number of threads in a block and
memory access time in a test.
Assuming a 512x512 sized image we have started a branch
of blocks with N threads inside for full coverage. The ﬁgure
shows that if we use vector-based access, we can write faster.
In the third case we had two read operations from the texture
before writing. Compared to the case when all threads are
trying to access the global memory we can archive a more
balanced utilization when the kernel has other kind of jobs to
work on during I/O operations. Even reading for the texture
due to the caching can be also hidden in writing cycles. To
achieve the best performance, we should use 64 threads per
block. We decided to use that number in order to keep more
space for additional blocks running on the same MP giving
more opportunity to the task scheduler for optimization.

Fig. 5. The ﬁgure shows that if we use vector-based access, we can write the
memory faster. Compared to the case when all threads are trying to access the
global memory we can archive a more balanced utilization when the kernel
has other kinds of operations to work on than writing.

V. EX PER IM EN TA L RE SU LT S

For our test results, we used an NVIDIA GeForce 8800
GT GPU connected via PCI Express 16x to an Intel Xeon
3.4 GHz CPU. Our applications ran atop Linux with NVIDIA
driver version 169.07 (64bit) and CUDA Tool kit 1.1. The
used MATLAB version was 2007b and SIMULINK was 7.0.
First we have made experiments with a 512x512 sized
image. The simulation included an image loading from hard-
drive, uploading to GPU, a template operation on GPU,
a downloading from GPU and a frame rate estimation.
SIMULINK’s built-in proﬁler has about 10 ms resolution, so
for fast running blocks it is quite unreliable, but the total time
measurement can be used as reference. If we have a large
iteration number for template running, the average iteration
time can be estimated more precisely. Furthermore, we found
in our speciﬁc case that it converges to a given value. This
number is the execution time of one iteration which equals
to 0.242 ms. This would allow us to do 4132 iterations
per second (IPS), but the inﬂuence of the additional blocks
give an overhead. We recorded the update speed measured
in frames per second (FPS) for this mini-algorithm with the
Frame Rate Display block in the function of iteration numbers.
(Fig. 6.) It is an overall speed indicator that also includes the
overhead. For comparison, we depicted the theoretical speed
limit deduced from the estimated pure template running time.
This overhead is about 20 ms and could be reduced if the
result is not downloaded to the host computer but displayed
using direct rendering on the video card.
We can compare this results to other simulators. For educa-
tional purposes, one of the most common tools is CANDY
from Analogic Computers Inc. It also offers an algorith-
mic framework with template iteration time of 21 ms for
an 512x512 sized image running on a 1.8 Ghz Pentium 4
computer. Considering this, a slightly more than 80x speed-
up was reached. A comprehensive comparison can be found
in [15] for the actual CNN simulators and emulators. We
extended the measured data with our results. (Table I.) The

Object Killer template to remove most of the noisy pixels.
Finally ,this mask will be used with the other thresholded one
to recall the deleted worm parts.

V I . CONC LU S ION

We have presented an algorithmic development frame-
work for designing and testing CNN algorithms using UMF
diagram-like notations. SIMULINK gives an environment for
handling various data streams and to create complex ﬂow
structures to build algorithms. With the additional blocks
for accessing GPU and running templates on it, we can
get an efﬁcient CNN simulator for an affordable price. We
have introduced our design consideration for optimal mapping
between the CNN and the GPU architecture, resulting in a
competitive speed that is ﬁve time faster than the cutting-edge
CPU implementation using high-level optimization. To apply

for this SIMCNN software package visit [18].

ACKNOW L EDG EM EN T S

The Ofﬁce of Naval Research (ONR),
the Operational
Program for Economic Competitiveness (GVOP KMA), the
NI 61101 Infobionics, Nanoelectronics, Artiﬁcial Intelligence
project, and the National Ofﬁce for Research and Technol-
ogy (NKTH RET 2004) which supports the multidisciplinary
doctoral school at the Faculty of Information Technology of
the Pázmány Péter Catholic University are gratefully acknowl-
edged. The authors are also grateful to Professor Tamás Roska,
Kristóf Karacs and the members of the Robotics lab for the
discussions and their suggestions. And special thanks go to
Gergely Lombai and Zsóﬁa Cserey.

R E F ER ENC E S

[1] Roska, T.; Chua, L.O., "The CNN universal machine: an analogic array
computer," Circuits and Systems II: Analog and Digital Signal Processing,
IEEE Transactions on [see also Circuits and Systems II: Express Briefs,
IEEE Transactions on] , vol.40, no.3, pp.163-173, Mar 1993
[2] Roska, T., "Computational and computer complexity of analogic cellular
wave computers," Cellular Neural Networks and Their Applications,
2002. (CNNA 2002). Proceedings of the 2002 7th IEEE International
Workshop on ,vol., no., pp. 323-338, 22-24 Jul 2002
[3] T. Roska, L. Kék, L. Nemes, A. Zarándy, M. Brendel, and P. Szolgay,
“CNN software library (templates and algorithms), version 7.2.,” Tech.
Rep. DNS-CADET-15, Analogical and Neural Computing Laboratory,
Computer and Automation Research Institute, Hungarian Academy of
Sciences (MTA SzTAKI), 1998.
[4] "World’s First
2-Billion Transistor Microprocessor
Intel
Corporation,

http:/www.intel.com/technology/
architecture- silicon/2billion.htm?iid=homepage+

- Tukwila",

news\_itanium\_platform; accessed February 24, 2008
[5] "NVIDIA CUDA Programming Guide 1.1", NVIDIA Corpora-

tion, http://developer.download.nvidia.com/compute/
cuda/1_1/NVIDIA_CUDA_Programming_Guide_1.1.pdf; ac-

cessed February 24, 2008
[6] H. Peter Hofstee, IBM Corporation, “Introduction to the Cell Broadband
Engine”
support&primitive=0; accessed February 24, 2008
[7] Technical Brief: “NVIDIA GeForce 8800 GPU Architecture Overview”
Santa Clara, California,November 2006. http://www.nvidia.com/

http://www.nvidia.com/attach/941771?type=

attach/941771?type=support&primitive=0; accessed Febru-

ary 24, 2008
[8] P. Dudek and P. Hicks, “A general-purpose processor-per-pixel analog
simd vision chip,” IEEE Transactions on Circuits and Systems - I: Analog
and Digital Signal Processing, vol. 520, no. 1, pp. 13-20, 2005.

Fig. 6. Execution speed in FPS of a single template running for N iteration is
displayed - measured value inﬂuenced by all necessary interface blocks with
strait line and theoretical maximum without any overhead with stripped.

ﬁrst two columns show results from Intel Core Duo and Quad
respectively using Intel Performance Library [16]. The next
one is the Cell Broadband Engine. It’s clear that the Cell is
the best but integration into our desktop system is not yet
supported. We have also placed an FPGA implementation in
the last column. We can state that our solution is ﬁve times
faster then the cutting-edge PC’s CPU.

COM PAR I SON O F TE ST R E SU LT S W I TH OTH ER IM PL EM EN TAT ION S

TABLE I

0
0
2
7

T

0
0
8
6

Q

L
L
E
C

T

G

0
0
8
8

y
d
n
a

C

0
0
0
5
S
3

C
X

Frequency (MHz)

2000

2933

3200

1350

1800

150

# of Processors

M cell iteration

Power (W)

2

48

34

4

100

130

8

3627

86

120

524

160

1

12

60

34

1700

10

If our system is powered by a 8800GT to realize a real-
time algorithm with 25 fps, we can include different templates
consuming 80 iterations. For demonstration purposes, we show
a feature extractor algorithm running on a video ﬂow on Fig 7.
It takes 50 iterations altogether allowing us to display the
intermediate results as well. The input is a noisy and slightly
off-focused video ﬂow captured via a microscope. The goal
is to distinguish between the elongated worms and the other
blood components. First, we create two thresholded maps with
different bias values. The ﬁrst contains most of the worms with
additional disturbation. The second one is more aggressively
thresholded containing less information about the worms, but
noise is also strongly suppressed. Than we apply the Small

Fig. 7. This ﬁgure shows a screenshot of the SIMCNN framework. It contains the UMF diagram-like ﬂow chart of a simple algorithm that can be run video
real-time. The upper left display shows the input image and is followed by the two thresholded intermediates, the one after running the Small Object Killer
and ﬁnally the reconstructed one that is the result of the algorithm. In the middle, the uploading and the downloading blocks are placed, all blocks among
them representing templates running on the GPU. Finally, in the bottom, the template block subsystem is displayed.

software/products/perflib; accessed February 24, 2008

[17] A. Fernandez, R. San Martin, E.Farguell and G.Egidio Pazienza, “Cel-
lular Neural Networks Implementation on a Parallel Graphics Processor
Unit,” Proceedings of the 11th International Workshop on Cellular Neural
Networks and their Applications, 2008
[18] Our SIMCNN software package, http://robotlab.itk.ppke.
hu/simcnn; accessed February 24, 2008

[9] P. Foldesy, A. Zarandy, C. Rekeczky, and T. Roska, “Digital implementa-
tion of cellular sensor-computers,” International Journal of Circuit Theory
and Applications, vol. 34, no. 4, pp. 409-428, 2006.
[10] A. Zarandy, P. Foldesy, P. Szolgay, S. Tokes, C. Rekeczky, and T.
Roska, “Various implementations of topographic, sensory, cellular wave
computers,” in Proceedings of the IEEE International Symposium on
Circuits and Systems, ISCAS 2005, (Kobe, Japan), pp. 5802-5805, May
2005.
[11] G. Linan, S. Espejo, R. Dominguez-Castro, and Rodriguez-Vazquez,
“ACE4k: an analog I/O 64 x 64 visual microprocessor chip with 7-bit
analog accuracy,” International Journal of Circuit Theory and Applica-
tions, vol. 30, no. 2-3, pp. 89-116, 2002.
[12] A. Rodriguez-Vazquez, G. Linan-Cembrano, L. Carranza, E. Roca-
Moreno, R. Carmona-Galan, F. Jimenez-Garrido, R. Dominguez-Castro,
and S. Meana, “ACE16k: the third generation of mixed-signal SIMD-CNN
ACE chips toward VSoCs,” IEEE Transactions on Circuits and Systems
Part I: Regular Papers, vol. 51, no. 5, pp. 851-863, 2004.
[13] R. Galan, F. Garrido, R. Castro, S. Meana, and A. Vazquez, “CACE1K
User’s Guide, A Complex Analog and Logic Computing Engine with
1024 Processing Cells,” Inst. de Microel. de Sevilla CNM CSIC, Jan,
vol. 23, 2002.
[14] A. Zarandy and C. Rekeczky, “Bi-i: a standalone ultra high speed cellular
vision system,” IEEE Circuits and Systems Magazine, vol. 5, no. 2, pp.
36-45,2005.
[15] Z. Nagy, “Implementation of emulated digital CNN-UM architecture on
programale logic devices and its applications,” PhD Thesis, Department of
Image Processing and Neurocomputing University of Pannonia Veszprém,
Hungary ,2007.
[16] Intel Performance Libraries Homepage, http://www.intel.com/

