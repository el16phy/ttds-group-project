688114 CDPXXX10.1177/0963721416688114Young, BurtonRecognizing Faces
research-ar ticle2017

Recognizing Faces

Andrew W. Young and A. Mike Burton

Department of Psychology, University of York

Current Directions in Psychological
Science
2017, Vol. 26(3) 212 –217
© The Author(s) 2017
Reprints and permissions: 
sagepub.com/journalsPermissions.nav
DOI: 10.1177/0963721416688114
www.psychologicalscience.org/CDPS

https://doi.org/10.1177/0963721416688114

Abstract
The idea that most of us are good at recognizing faces permeates everyday thinking and is widely used in the research 
literature. However, it is a correct characterization only of familiar-face recognition. In contrast, the perception and 
recognition of unfamiliar faces can be surprisingly error-prone, and this has important consequences in many real-life 
settings. We emphasize the variability in views of faces encountered in everyday life and point out how neglect of this 
important property has generated some misleading conclusions. Many approaches have treated image variability as 
unwanted noise, whereas we show how studies that use and explore the implications of image variability can drive 
substantial theoretical advances.

Keywords
face recognition, face matching, face perception, familiar faces, unfamiliar faces

A widely held opinion is that most people are good at 
recognizing faces. Politicians call for citizens to carry 
photo ID, eyewitness evidence can seem very compel-
ling in court, and researchers say that human adults are 
“face experts” (Carey, 1992). Although it has become well 
established that some people have problems with face 
recognition, these individuals are typically assumed to 
form a minority suffering from some form of pathological 
“face blindness” (Behrmann & Avidan, 2005), in which 
normally excellent face-recognition abilities are some-
how missing or switched off.
Our aim here is to review classic and recent findings 
that show the limitations of the above characterization of 
human face recognition and replace it with something 
that is more securely grounded in evidence. This evi-
dence has often been overlooked because it doesn’t fit 
neatly into a set of assumptions that seem like common 
sense. We will outline serious problems inherent in the 
“common-sense” way of thinking and offer a more firmly 
established account and explanation of why we research-
ers have been misleading ourselves.
Modern research on face recognition can be traced 
back to the seminal review by Ellis (1975), which brought 
together a wide range of studies of normal adults, infants, 
and children and studies of the effects of brain injury to 
establish face recognition as a distinct field of psycho-
logical enquiry. We have borrowed the title of that work, 
Recognizing Faces, to emphasize our indebtedness to 
Ellis (1975), but as we will explain, much has also 
changed across the intervening 40 years.

Face Recognition and Picture 
Recognition

Ellis (1975) noted remarkably high levels of correct recog-
nition of faces in standard recognition memory tasks. In 
such tasks, a participant studies a series of photographs of 
faces of people he or she has never seen before. We will 
call these unfamiliar faces to distinguish them from faces 
of already known individuals. These photographs of stud-
ied unfamiliar faces are then mixed with photographs of 
other unfamiliar faces, and the participant is asked to pick 
out the faces seen in the study phase of the experiment. 
Ellis’s (1975) review (and many subsequent studies) 
showed that high levels of performance can be found on 
such tasks even when the faces were studied for only a 
few seconds each, and that performance is better for faces 
than for many other types of visual stimuli and better for 
upright than for inverted faces.
These laboratory findings contrasted markedly with 
miscarriages of justice in the 1970s in which eyewitnesses 
were found to have misidentified the perpetrators of vari-
ous crimes. In some of these cases, witnesses were mis-
taken despite being certain that they were correct. The 
apparent paradox of the discrepancy between excellent 
laboratory performance (in recognition memory tasks) 

Corresponding Author:
Andy Young, Department of Psychology, University of York, 
Heslington, York YO10 5DD, United Kingdom 
E-mail: andy.young@york.ac.uk

Recognizing Faces 

213

Fig. 1. Two sets of example stimuli used in the face-matching task from Bruce et al. (1999). The lower arrays show photographs of 10 faces, 
and the upper photographs were taken on the same day but with a different camera. Are the top people present in the lower arrays, or are 
they missing? Solutions are given at the end of this article.

and fragile real-world performance (in eyewitness mis-
identifications) was resolved by Bruce (1982), who pointed 
out that standard recognition memory tasks asked partici-
pants to pick out face photographs that were identical to 
the ones they had studied. This procedure is as much one 
of picture recognition as of face recognition. A true face-
recognition test should require that participants recognize 
the studied faces across different photographs. It turns out 
that for faces participants have seen only in a single pho-
tograph, generalization of recognition to new images is 
poor; performance falls off quickly as the difference 
between the studied and test photographs increases, even 
for face photographs that were very thoroughly learned 
(Longmore, Liu, & Young, 2008). In consequence, the 
general applicability of laboratory face-recognition per-
formance was overestimated because of researchers’ reli-
ance on picture-recognition tasks.
The difference between picture recognition and face 
recognition is easy to grasp, but it took researchers much 
longer to appreciate that humans’ relatively poor perfor-
mance at unfamiliar-face recognition is as much a prob-
lem of perception as of memory. Try for yourself the 
face-matching task shown in Figure 1. Even though the 
orientations and expressions of the faces are much the same 
(and the pictures were taken on the same day so that 
there were no changes in hairstyle or other modifiable 
characteristics), most people find the task surprisingly 

tricky—with overall error rates around 30% (Bruce et al., 
1999). Yet this is not a memory task; it is purely one of 
perceptual matching, and viewers can take as long as 
they like to compare the images.
It might be thought that the task shown in Figure 1 is 
unfair because it involves an artificially contrived prob-
lem of comparing one photograph to another, but in fact 
the same problems arise for photo-based face identifica-
tion in everyday life. Kemp, Towell, and Pike (1997) con-
ducted a study in a supermarket, using specially created 
ID cards with either a valid photograph (i.e., an actual 
photo of the person) or an invalid photograph (i.e., a 
photo of another person). The supermarket’s own 
cashiers were told to look out for the fake ID cards and 
told that a bonus payment would be made to the cashier 
with the best performance. Yet the cashiers challenged 
around 10% of people presenting a valid card and accepted 
64% of the invalid cards if there was some similarity in 
appearance between the card presenter and the person 
actually pictured. Thus, even comparing a photo of a face 
to the person standing in front of you is not as easy as we 
usually take it to be.
It turns out that unfamiliar-face matching is also a 
problem for professional groups that rely on this ability. 
For example, White, Kemp, Jenkins, Matheson, and Burton 
(2014) showed that working passport officers had an 
error rate of about 10% when asked to verify passport 

214 

Young, Burton

Fig. 2. An example of the task used by Jenkins, White, Van Montfort, and Burton (2011). Can you sort the 40 images into different face identities? 
(The solution is given at the end of this article.) Most people arrive at the correct solution only if they already know the faces. For unfamiliar faces, 
participants tend to mistake differences between the images for differences in identity, leading them to overestimate the number of faces in the dis-
play. With familiar faces, the task becomes easy. Reprinted from “Variability in Photos of the Same Face,” by R. Jenkins, D. White, X. van Montfort, 
and A. M. Burton, 2011, Cognition, 121, p. 316. Copyright 2011 by Elsevier. Reprinted with permission.

photos of volunteers approaching an ID check. Despite 
extensive training, highly experienced officers performed, 
on average, no better than new recruits.
These average error rates actually conceal large indi-
vidual variation, however. Most studies have shown very 
little effect of training on people’s unfamiliar-face-matching 
skills, but there appear to be substantial, and stable, differ-
ences between people in their baseline abilities. These 
individual differences in face-matching abilities are of con-
siderable interest in themselves (Yovel, Wilmer, & Duchaine, 
2014). In the present context, they have led to an emphasis 
on selection, rather than training, in professional settings. 
Large organizations have the opportunity to select staff 
with particularly high face-matching ability for face-related 
tasks (e.g., the London Metropolitan Police Force; see 
Robertson, Noyes, Dowsett, Jenkins, & Burton, 2016).

The Importance of Image Variability

The findings from unfamiliar-face-matching and photo-
ID tasks show that people have some kind of perceptual 
problem with recognizing the identities of unfamiliar 
faces, or at least with recognizing the identities of unfa-
miliar faces in photographs. Though there are substantial 

individual differences, and a few “super-recognizers”  
do show unusually high ability (Russell, Duchaine, & 
Nakayama, 2009), for most people performance is both 
error-prone and surprisingly hard to improve.
A powerful insight into the nature of this problem 
derives from a sorting task devised by Jenkins, White, 
Van Montfort, and Burton (2011). Participants were given 
a set of 40 photographs of faces like those shown in Fig-
ure 2 and asked to sort these into piles of photographs of 
the same person.
In fact, only two faces are shown in Figure 2, and any-
one who knows these two people will experience little 
trouble in creating a fully correct solution of two piles of 20 
images each ( Jenkins et al., 2011). However, participants in 
Jenkins et al.’s (2011) study, who were unfamiliar with 
these people, sorted their pictures into anywhere from 
three to 16 different piles (identities), with nine piles being 
the most common number. In other words, participants 
typically thought that the set of 40 photos depicted nine 
different individuals when there were actually only two. In 
marked contrast, people rarely put photos of the two differ-
ent individuals into the same pile (less than 1% of trials).
This finding is particularly interesting because it runs 
counter to a widely accepted intuition (which can be 

Recognizing Faces 

215

traced back at least as far as Galton, 1883) that faces form 
a homogeneous class of visual stimuli and that, in conse-
quence, people mainly struggle to tell similar faces apart. 
Rather, Jenkins et al.’s (2011) data show that participants 
are likely to see photos of faces as more diverse than 
they actually are (thus, they create too many piles). The 
problem is as much one of seeing that very different 
images can represent the same unfamiliar face identity as 
it is one of telling faces apart (Andrews, Jenkins, Cursiter, 
& Burton, 2015).
Jenkins et al. (2011) discussed their findings in terms 
of the idea of image variability. Photographs of faces dif-
fer in many ways that include pose, expression, lighting, 
camera, and lens characteristics. Importantly, real-life 
views of faces are also highly variable; this is true whether 
the faces are seen in person, in videos, or in photographs. 
This variability might result from within-person variability 
(e.g., differences between different views of the same 
face) or between-person variability (e.g., differences 
between similar views of different faces). As Figure 2 
clearly shows, within-person variability can be substan-
tial. Researchers and government authorities therefore try 
to minimize its impact through the creation of sets of 
highly standardized images for laboratory tasks or pass-
port photographs. This tactic is at best of limited use; the 
findings we have discussed show that image variability 
can easily confuse the visual system when people look at 
unfamiliar faces.

(Bruce & Young, 1986; Burton, Bruce, & Hancock, 1999; 
Schweinberger & Burton, 2011; Young & Bruce, 2011; 
Young & Burton, 1999). Image variability is generally not 
a problem when it comes to familiar faces, which can  
be recognized despite even severe image degradation 
(Burton, Jenkins, & Schweinberger, 2011).
Why should this be? In essence, the answer seems to 
be that we have seen familiar faces enough times to have 
learned how to cope with their variability (Longmore 
et al., 2008), but can we arrive at something more pre-
cise? Bruce (1994) introduced the idea that “stability from 
variation” might be useful for face recognition: Multiple 
exposures to a familiar face allow our perceptual systems 
to separate transient within-person differences from the 
stable characteristics of that face. Developing this insight, 
Burton, Jenkins, Hancock, and White (2005) offered a 
technique that can capture what is consistent across dif-
ferent images of the same person’s face by using com-
puter image manipulation to average together multiple 
examples (see Fig. 3). This within-person average of 
someone’s face turns out to be an excellent representa-
tion for automatic computer-based recognition ( Jenkins 
& Burton, 2008) and has attractive properties for human 
perception, too. Averaging leads to a robust representa-
tion that is not unduly influenced by the superficial dif-
ferences that make simple image comparison so difficult 
(e.g., lighting changes; Jenkins & Burton, 2011).

The Importance of Face Familiarity

Although unfamiliar faces can present significant prob-
lems, these problems are remarkably negligible for famil-
iar faces. This contrast between familiar- and unfamiliar-face 
recognition has been at the heart of cognitive models 

Image Variability and Face Familiarity

Although averaging offers a powerful way to find the 
consistent cues that signal a face’s identity, it seems curi-
ous that the visual system remains confused by variability 
between images of unfamiliar faces, given one’s expo-
sure to so many faces in everyday life. The reason turns 

Fig. 3. Fourteen different photographs of one of the authors (A. M. Burton) and a computer-manipulated average of these (the larger image 
shown to the right of the 14 photos; a), and an average image of a face that should be familiar to many readers (Harrison Ford) created with an 
equivalent procedure (b). Note how effective averaging is in removing the impact of identity-irrelevant differences in lighting, pose, and expres-
sion. Panel (a) is adapted from “Stable Face Representations,” by R. Jenkins and A. M. Burton, Philosophical Transactions of the Royal Society B: 
Biological Sciences, 366, p. 1677. Copyright 2011 by The Royal Society. A version of this figure is available at http://rstb.royalsocietypublishing 
.org/content/366/1571/1671 and may be reproduced under a Creative Commons Attribution license (https://creativecommons.org/licenses/by/4.0/).

216 

Young, Burton

out to be that the variability is to some extent idiosyn-
cratic—that is, the ways in which Face A varies across 
different images need not be the same as the ways in 
which Face B varies across different images. Burton, 
Kramer, Ritchie, and Jenkins (2016) analyzed the statisti-
cal properties of face images both within and between 
identities. Consistent with previous findings, the largest 
variations were common to all faces and corresponded to 
physical differences such as pose and lighting direction. 
Once this common variation was removed, the remaining 
differences among images of the same person were 
highly person-specific.
This idiosyncratic variability of faces explains how we 
can be an “expert” at recognizing one face but not another 
(Burton et al., 2016; Kramer, Young, Day, & Burton, 2017). 
Learning how the face of Brad Pitt can vary—through 
seeing him in many settings—allows us to recognize new 
photos of him even in quite novel settings (e.g., if he is 
caught with an unusual expression or in poor lighting). 
This expertise may not generalize to another person’s 
face, however, because that person will vary in different 
ways. For this reason, it is very easy to match two photos 
of a familiar person but very difficult to match two pho-
tos of an unfamiliar person, whose range of variability is 
unknown. It also explains why attempts to train people 
to become better unfamiliar-face matchers in general are 
likely to fail. We can train observers to recognize a par-
ticular new face very well, but this training will not gen-
eralize to recognizing a different face (see Dowsett, 
Sandford, & Burton, 2016).

Image Variability and the Concept  
of Expertise

Where do these findings leave us? A good way to begin 
considering their broader implications is to return to the 
idea that people are face experts, and the more general 
concept of visual expertise. This is usually put forward 
in terms of expertise in face recognition, but, as we 
have explained, such a generic concept often does not 
hold. For face-identity recognition, people mainly show 
something that can be considered expertise only for 
those faces they know well. One can be an expert at 
recognizing Brad Pitt, Barack Obama, or Scarlett Johans-
son, but this expertise is identity-specific and does not 
generalize to recognizing the identities of unfamiliar 
faces. It is the ease with which people recognize famil-
iar faces that seems to have misled researchers into 
overlooking limitations with unfamiliar faces (Ritchie 
et al., 2015).
That said, there are other aspects of unfamiliar-face 
perception in which people do show expertise. For 
example, people can usually make a reasonably accurate 
estimate of a person’s age or sex from his or her face, and 

they can interpret subtle differences in facial expressions 
and gaze direction (see Bruce & Young, 2012). What 
characterizes such abilities is that, unlike face recogni-
tion, they involve cues that are highly consistent across 
many different faces (Kramer et al., 2017).
From findings such as those we have presented, it is 
clear that considering image variability is critical to under-
standing face recognition. A key corollary of this is that 
we need to study naturally occurring images of faces, 
which Jenkins et al. (2011) have called ambient images. 
The traditional approach to face recognition has been to 
use highly standardized images in an attempt to minimize 
the impact of “nuisance” variation due to factors such as 
lighting and camera differences. However, we have 
shown that this approach can obscure the understanding 
of face recognition. By using the full range of ambient 
images of the type people encounter every day in newspa-
pers, televisions, and online, we can preserve the natural 
within-person variations that characterize our real-world 
experience of faces. Far from being a nuisance, this vari-
ability is a necessity in allowing us to find consistent cues 
for recognizing face identity (Bruce, 1994; Burton, 2013) 
and for other aspects of face perception (Bruce & Young, 
2012; Sutherland et al., 2013; Vernon, Sutherland, Young, 
& Hartley, 2014).

Solutions to Face-Matching Problems 

Figure 1. In the array in (a), the target image is Face 3. The 
target is absent in the array in (b).

Figure 2. There are two identities, arranged as follows:
ABAAABABAB
AAAAABBBAB
BBBAAABBAA
BABAABBBBB

Acknowledgments

We are grateful to Nicholas Rule and three anonymous review-
ers for helpful comments and suggestions.

Recommended Reading

Bruce, V., & Young, A. (2012). (See References). A more exten-
sive treatment that puts recognition into the context of a 
range of questions and issues concerning face perception.
Burton, A. M. (2013). (See References). A more detailed discus-
sion of some of the key points made here.
Burton, A. M., Kramer, R. S. S., Ritchie, K. L., & Jenkins, R. 
(2016). (See References). Analyzes the variability across 
images of faces to demonstrate why each familiar face has 
to be individually learned.
Dowsett, A. J., Sandford, A., & Burton, A. M. (2016). (See 
References). Shows how image variability can enhance 
learning of specific faces.
White, D., Kemp, R. I., Jenkins, R., Matheson, M., & Burton, A. M. 
(2014). (See References). Shows that passport officers vary 

Recognizing Faces 

217

in their ability to perform unfamiliar-face-matching tasks and 
that this variation is not related to experience or training.

Declaration of Conflicting Interests

The authors declared that they had no conflicts of interest with 
respect to their authorship or the publication of this article.

Funding

The research leading to these results has received funding from 
the European Research Council (ERC) under the European 
Union’s Seventh Framework Programme (2007–2013) through 
ERC Grant Agreement n.323262.

References

Andrews, S., Jenkins, R., Cursiter, C., & Burton, A. M. (2015). 
Telling faces together: Learning new faces through exposure 
to multiple instances. Quarterly Journal of Experimental 
Psychology, 68, 2041–2050.
Behrmann, M., & Avidan, G. (2005). Congenital prosopagno-
sia: Face-blind from birth. Trends in Cognitive Sciences, 9, 
180–187.
Bruce, V. (1982). Changing faces: Visual and non-visual coding 
processes in face recognition. British Journal of Psychology, 
73, 105–116.
Bruce, V. (1994). Stability from variation: The case of face rec-
ognition. Quarterly Journal of Experimental Psychology, 47, 
5–28.
Bruce, V., Henderson, Z., Greenwood, K., Hancock, P. J. B., 
Burton, A. M., & Miller, P. (1999). Verification of face identi-
ties from images captured on video. Journal of Experimental 
Psychology: Applied, 5, 339–360.
Bruce, V., & Young, A. (1986). Understanding face recognition. 
British Journal of Psychology, 77, 305–327.
Bruce, V., & Young, A. (2012). Face perception. Hove, England: 
Psychology Press.
Burton, A. M. (2013). Why has research in face recognition pro-
gressed so slowly? The importance of variability. Quarterly 
Journal of Experimental Psychology, 66, 1467–1485.
Burton, A. M., Bruce, V., & Hancock, P. J. B. (1999). From pixels 
to people: A model of familiar face recognition. Cognitive 
Science, 23, 1–31.
Burton, A. M., Jenkins, R., Hancock, P. J. B., & White, D. (2005). 
Robust representations for face recognition: The power of 
averages. Cognitive Psychology, 51, 256–284.
Burton, A. M., Jenkins, R., & Schweinberger, S. R. (2011). 
Mental representations of familiar faces. British Journal of 
Psychology, 102, 943–958.
Burton, A. M., Kramer, R. S. S., Ritchie, K. L., & Jenkins, R. (2016). 
Identity from variation: Representations of faces derived 
from multiple instances. Cognitive Science, 40, 202–223.
Carey, S. (1992). Becoming a face expert. Philosophical Trans-
actions of the Royal Society B: Biological Sciences, 335, 95–
103.
Dowsett, A. J., Sandford, A., & Burton, A. M. (2016). Face learning 
with multiple images leads to fast acquisition of familiarity 

for specific individuals. Quarterly Journal of Experimental 
Psychology, 69, 1–10.
Ellis, H. D. (1975). Recognizing faces. British Journal of 
Psychology, 66, 409–426.
Galton, F. (1883). Inquiries into human faculty and its develop-
ment. London, England: Macmillan.
Jenkins, R., & Burton, A. M. (2008). 100% accuracy in automatic 
face recognition. Science, 319, 435.
Jenkins, R., & Burton, A. M. (2011). Stable face representations. 
Philosophical Transactions of the Royal Society B: Biological 
Sciences, 366, 1671–1683.
Jenkins, R., White, D., van Montfort, X., & Burton, A. M. (2011). 
Variability in photos of the same face. Cognition, 121, 313–
323.
Kemp, R., Towell, N., & Pike, G. (1997). When seeing should 
not be believing: Photographs, credit cards and fraud. 
Applied Cognitive Psychology, 11, 211–222.
Kramer, R. S. S., Young, A. W., Day, M. G., & Burton, A. M. 
(2017). Robust social categorization emerges from learning 
the identities of very few faces. Psychological Review, 124, 
115–129.
Longmore, C. A., Liu, C. H., & Young, A. W. (2008). Learning faces 
from photographs. Journal of Experimental Psychology: 
Human Perception and Performance, 34, 77–100.
Ritchie, K. L., Smith, F. G., Jenkins, R., Bindemann, M., White, 
D., & Burton, A. M. (2015). Viewers base estimates of face 
matching accuracy on their own familiarity: Explaining the 
photo-ID paradox. Cognition, 141, 161–169.
Robertson, D. J., Noyes, E., Dowsett, A. J., Jenkins, R., & Burton, 
A. M. (2016). Face recognition by Metropolitan Police 
super-recognisers. PLoS ONE, 11(2), e0150036. doi:10.1371/
journal.pone.0150036
Russell, R., Duchaine, B., & Nakayama, K. (2009). Super-
recognizers: People with extraordinary face recognition 
ability. Psychonomic Bulletin & Review, 16, 252–257.
Schweinberger, S. R., & Burton, A. M. (Eds.). (2011). Person 
perception 25 years after Bruce and Young (1986) [Special 
issue]. British Journal of Psychology, 102, 695–974.
Sutherland, C. A. M., Oldmeadow, J. A., Santos, I. M., Towler, J., 
Burt, D. M., & Young, A. W. (2013). Social inferences from 
faces: Ambient images generate a three-dimensional model. 
Cognition, 127, 105–118.
Vernon, R. J. W., Sutherland, C. A. M., Young, A. W., & Hartley, T. 
(2014). Modeling first impressions from highly variable facial 
images. Proceedings of the National Academy of Sciences, 
USA, 111, E3353–E3361.
White, D., Kemp, R. I., Jenkins, R., Matheson, M., & Burton, A. 
M. (2014). Passport officers’ errors in face matching. PLoS 
ONE, 9(8), e103510. doi:10.1371/journal.pone.0103510
Young, A. W., & Bruce, V. (2011). Understanding person per-
ception. British Journal of Psychology, 102, 959–974.
Young, A. W., & Burton, A. M. (1999). Simulating face rec-
ognition: Implications for modelling cognition. Cognitive 
Neuropsychology, 16, 1–48.
Yovel, G., Wilmer, J. G., & Duchaine, B. (2014). What can individual 
differences reveal about face processing? Frontiers in Human 
Neuroscience, 8, Article 562. doi:10.3389/fnhum.2014.00562

