# -*- coding: utf-8 -*-
from readContent import readContent
from Lemmatizer import Lemmatizer
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
from readXML import readXML
from readNEWS import readNEWS
from random import shuffle
import pandas as pd
import numpy as np
import operator
import math
import re
import datetime
import time


def Calculate(lent, stopWords):
    score = 0.0
    RealWords = 0
    HardWords = 0

    # FinalWords = 0
    # FinalScore = 0.0

    for i in lent:
            RealWords += 1
            # FinalWords += 1
            if(TermDict.has_key(str.lower(str(i)))):
                score += float(TermDict[str.lower(str(i))])
                if(float(TermDict[str.lower(str(i))]) > 0.1):
                    HardWords += 1
            else:
                TermDict[str.lower(str(i))] = MaxValue
                score += MaxValue
                HardWords += 1


            if(DocDict.has_key(str.lower(str(i)))):
                DocDict[str(i)] += 1
            else:
                DocDict[str(i)] = 1

    return score, RealWords, HardWords

E = readXML()
C = readContent()
N = readNEWS()
L = Lemmatizer()
TermDict = dict()
DocDict = dict()
TermIDDict = dict()
stopWords = set(stopwords.words('english'))


# with open("TermFreuqency_WithStopWords5w_V4.txt", "r") as f:
MaxValue = 2.0
with open("TermFreuqency_WithStopWords S4 8w5 0_00007.txt", "r") as f:
    content = f.readlines()
f.close()
# print(content)
for i in content:
    TermDict[(i.split("\t"))[0]] = ((i.split("\t"))[2]).strip()

for i in content:
    TermIDDict[((i.split("\t"))[0])] = int(((i.split("\t"))[1]).strip())


f2 = open("SVM FIX Test.txt", "w")

amount = 300

content = """op drone maker DJI announced that it has fired some of its employees after uncovering fraud cases that could cost it tens of millions of dollars.
The private Chinese company, the world's biggest producer of consumer drones, said in a statement Monday that an internal investigation found employees had "inflated the cost of parts and materials for certain products for personal financial gain."
The incident raises questions about the financial management of one of China's best known technology firms, which sells billions of dollars of drones around the world each year.

DJI said in a statement it has contacted law enforcement officials about the case. The company also said it would strengthen its internal controls and establish "new channels for employees to submit confidential and anonymous reports relating to any violations of the company's workplace conduct policies" in the aftermath of the incident.
Inside China&#39;s Silicon Valley: From copycats to innovation
Inside China's Silicon Valley: From copycats to innovation
Reports put the number of employees it fired at 29 and the size of the potential losses from the fraud at $150 million.
A spokeswoman for DJI declined to confirm those numbers to CNN.
"We continue to investigate the situation and are cooperating fully with law enforcement's investigation," the company said in its statement.
Police in Shenzhen, the city where DJI is based, didn't immediately respond to a request for comment.
DJI, which employs around 14,000 people, became one of China's most successful homegrown technology companies by pioneering the global market for consumer drones.
DJI sells millions of drones around the world every year.
DJI sells millions of drones around the world every year.
CEO Frank Wang, who founded the startup in 2006 while studying at a university in Hong Kong, was named the first "drone billionaire" by Forbes.

Today, the company has cornered nearly three-quarters of the consumer drone market, according to industry research firm Skylogic Research, and is seen by analysts as significantly ahead of its peers.
One of its rivals, GoPro, quit the drone market last year, citing difficulties with regulators and an extremely competitive market. """

if(len(content.split()) > 200):
    lent = content.split() # split the terms
    content = E.CheckEnglish(content.strip())
    content = L.RunLemmatizer(content)


    lent = content.split()
    CleanTerm = []
    for i in lent:
        if str.lower(str(i)) not in stopWords:
            CleanTerm.append(str.lower(str(i)))
    shuffle(CleanTerm)
    if(len(CleanTerm) < amount):
        S1, R1, H1 = Calculate(CleanTerm, stopWords)
        S2 = S1
        S3 = S1
        R2 = R1
        R3 = R1
        H2 = H1
        H3 = H1
    else:
        Test1 = CleanTerm[:amount]
        Test2 = CleanTerm[int(len(CleanTerm)/2):int(len(CleanTerm)/2)+amount]
        Test3 = CleanTerm[len(CleanTerm)-amount:]
        S1, R1, H1 = Calculate(Test1, stopWords)
        S2, R2, H2 = Calculate(Test2, stopWords)
        S3, R3, H3 = Calculate(Test3, stopWords)





    score = (S1+S2+S3)
    RealWords = (R1+R2+R3)
    HardWords = (H1+H2+H3)

    SortedDocDict = sorted(DocDict.items(), key=operator.itemgetter(1), reverse=True)
    # print(SortedDocDict)
    countWords = 0


    f2.write("4\t")

    SVM = dict()
    for i in SortedDocDict:
        if(TermIDDict.has_key(i[0])):
            if(SVM.has_key(int(TermIDDict[i[0]]))):
                SVM[int(TermIDDict[i[0]])] += int(i[1])
            else:
                SVM[int(TermIDDict[i[0]])] = int(i[1])
        else:
            TermIDDict[i[0]] = 1
            if(SVM.has_key(int(TermIDDict[i[0]]) )):
                SVM[int(TermIDDict[i[0]])] += int(i[1])
            else:
                SVM[int(TermIDDict[i[0]])] = int(i[1])



        countWords += 1

        if((countWords > 100)):

            SortedDocDict = []
            DocDict.clear()
            break

    sorted_SVM = sorted(SVM.items(), key=operator.itemgetter(0), reverse = False)

    for j in sorted_SVM:
        f2.write(str(j[0])+":"+str(j[1])+" ")



    f2.write("\t#4\n")
    score /=3.0
    RealWords /= 3.0
    HardWords /= 3.0
    SVM.clear()



f2.close()
# The stop word count as part of 500 words, but it did not count the score
