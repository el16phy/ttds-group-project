# -*- coding: utf-8 -*-
from readContent import readContent
from readFan import readFan

from Lemmatizer import Lemmatizer
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
from readXML import readXML
from readNEWS import readNEWS
from random import shuffle
import pandas as pd
import numpy as np
import operator
import math
import re
import datetime
import time


def takeSecond(elem):
    lis = []
    for i in elem:
        print(i)
        # print(i.split())
        # lis.append(i.split()[1])
    return elem[1]



def Calculate(lent, stopWords):
    score = 0.0
    RealWords = 0
    HardWords = 0

    # FinalWords = 0
    # FinalScore = 0.0

    for i in lent:
            RealWords += 1
            # FinalWords += 1
            if(TermDict.has_key(str.lower(str(i)))):
                score += float(TermDict[str.lower(str(i))])
                if(float(TermDict[str.lower(str(i))]) > 0.1):
                    HardWords += 1
            else:
                TermDict[str.lower(str(i))] = MaxValue
                score += MaxValue
                HardWords += 1


            if(DocDict.has_key(str.lower(str(i)))):
                DocDict[str(i)] += 1
            else:
                DocDict[str(i)] = 1

    return score, RealWords, HardWords

E = readXML()
C = readContent()
N = readNEWS()
L = Lemmatizer()
F = readFan()
TermDict = dict()
DocDict = dict()
TermIDDict = dict()

fig, ax = plt.subplots()
# plt.xticks(x_range)
x = ["0","1","2","3","4","5", "ADULT"]

ax.xaxis.set_ticklabels(x)
stopWords = set(stopwords.words('english'))


# with open("TermFreuqency_WithStopWords5w_V4.txt", "r") as f:
MaxValue = 2.0
with open("TermFreuqency_WithStopWords S4 8w5 0_00007.txt", "r") as f:
    content = f.readlines()
f.close()
# print(content)
for i in content:
    # print()
    TermDict[(i.split("\t"))[0]] = ((i.split("\t"))[2]).strip()

for i in content:
    # print()
    TermIDDict[((i.split("\t"))[0])] = int(((i.split("\t"))[1]).strip())
# print(TermDict)

# fo = open("/afs/inf.ed.ac.uk/user/s18/s1852861/Desktop/INFR11145/Group_Project/ttds-group-project/Word Frequency/Example Text/ExampleText4.txt", "r")
# content = str(fo.read())
# content = (re.sub('\d+',' ',content)).strip() # remove all digit
# content = content.replace("\n", " ")
#
# fo.close()

# fo = open("Data5w_V2_Result_V44.txt", "w")
# fo = open("Data5w_V2_Result_V41.txt", "w")
# fo = open("Data5w_V2_ARTICLE_Result_V43.txt", "w")
fo = open("100 Features FIX FINAL Test.txt", "w")
f2 = open("SVM 100 Features FIX FINAL Test.txt", "w")
amount = 300

# fil = ["data_2.xml", "data_1.xml"]
# fil = ["data_2.xml"]
fil = ["ResearchArticle.xml","NEWS.xml"]
fil = ["00.xml","01.xml", "03.xml", "04.xml"]
fil = ["00.xml","01.xml", "03.xml", "04.xml", "news7.xml","news6.xml", "news5.xml", "news4.xml"]
fil = ["ResearchArticle.xml","NEWS.xml", "data_2.xml", "data_1.xml", "00.xml","01.xml", "03.xml", "04.xml", "11.xml", "news7.xml","news6.xml", "news5.xml", "news4.xml"]
count = 0
average = dict()
average["0"] = 0.0
average["1"] = 0.0
average["2"] = 0.0
average["3"] = 0.0
average["4"] = 0.0
average["5"] = 0.0
average["ALL"] = 0.0
average["ADULT"] = 0.0


std = dict()
# std["0"] = []
# std["1"] = []
# std["2"] = []
# std["3"] = []
# std["4"] = []
# std["5"] = []
# std["ALL"] = []
# std["ADULT"] = []

fileNumber = 0
for fi in fil:
    fileNumber +=1
    data = []
    Data = []
    if(fileNumber == 1 or fileNumber == 2):
        data = N.Run(fi) # article or news
    elif(fileNumber >= 5 and fileNumber <=9):
        data = F.Run(fi)
    elif(fileNumber >= 10):
        data = F.Run(fi)
    else:
        data = C.Run(fi) #novel content

    Data = (sorted(data, key=lambda x: x[2]))
    # count = 0
    for D in Data:
        if(count < -1):
            break
        else:
            count += 1
            # print(D)
            if(fileNumber == 1 or fileNumber == 2):
                content = D[3] + " "+ D[1]# 3 for news, 4 for novel
                if(fileNumber == 1):
                    D[2] = "5"
                elif(fileNumber == 2):
                    D[2] = "ADULT"
            elif(fileNumber >= 5 and fileNumber <=9):
                D[2] = "5"
                content = D[3]
            elif(fileNumber >= 10):
                D[2] = "ADULT"
                content = D[3]
            else:
                if(str(D[2]) == "ALL"):
                    D[2] = "ADULT"
                content = D[4] # 3 for news, 4 for novel


            if(len(content.split()) > 200):
                lent = content.split() # split the terms
                content = E.CheckEnglish(content.strip())
                content = L.RunLemmatizer(content)
                # for i in content.split():
                #     DocDict[i] = 0
                # print(len(DocDict))

                lent = content.split()
                CleanTerm = []
                for i in lent:
                    if str.lower(str(i)) not in stopWords:
                        CleanTerm.append(str.lower(str(i)))
                shuffle(CleanTerm)
                if(len(CleanTerm) < amount):
                    S1, R1, H1 = Calculate(CleanTerm, stopWords)
                    S2 = S1
                    S3 = S1
                    R2 = R1
                    R3 = R1
                    H2 = H1
                    H3 = H1
                else:
                    Test1 = CleanTerm[:amount]
                    Test2 = CleanTerm[int(len(CleanTerm)/2):int(len(CleanTerm)/2)+amount]
                    Test3 = CleanTerm[len(CleanTerm)-amount:]
                    S1, R1, H1 = Calculate(Test1, stopWords)
                    S2, R2, H2 = Calculate(Test2, stopWords)
                    S3, R3, H3 = Calculate(Test3, stopWords)





                score = (S1+S2+S3)
                RealWords = (R1+R2+R3)
                HardWords = (H1+H2+H3)

                SortedDocDict = sorted(DocDict.items(), key=operator.itemgetter(1), reverse=True)
                # print(SortedDocDict)
                countWords = 0
                opp1 = True
                opp2 = True
                if(str(D[2]) == "0" or str(D[2]) == "1" or str(D[2]) == "ADULT"):
                    f2.write("1\t")
                elif(str(D[2]) == "2" or str(D[2]) == "3"):
                    f2.write("2\t")
                elif(str(D[2]) == "4" or str(D[2]) == "5"):
                    f2.write("3\t")

                SVM = dict()
                for i in SortedDocDict:
                    if(TermDict.has_key(str(i[0]))):
                        fo.write(str(i[0]) + ",\t" + str(i[1]) + ",\t" + str(TermDict[i[0]]) + "\n")
                        # f2.write(str(TermIDDict[i[0]]) +":"+ str(i[1]) +" ")

                        # print(str(i[0]) + ",\t" + str(i[1]) + ",\t" + str(TermDict[i[0]]) + "\n")
                        # if(str(round(float(TermDict[i[0]]),5)) not in SVM):
                        #     SVM.append(str(round(float(TermDict[i[0]]),5)))

                        if((TermDict[i[0]] > (MaxValue/2.0)) and (opp1 or opp2)):
                            score -= (float(i[1])*float(TermDict[i[0]]))
                            if(opp1):
                                opp1 = False
                            elif(opp2):
                                opp2 = False


                    else:
                        fo.write(str(i[0]) + ",\t" + str(i[1]) + ",\t" + str(MaxValue) + "\n")
                        # f2.write(str(MaxValue) +":"+ str(i[1]) +" ")

                        # if(str(MaxValue) not in SVM):
                        #     SVM.append(str(MaxValue))

                        if((TermDict[i[0]] > (MaxValue/2.0)) and (opp1 or opp2)):
                            score -= (float(i[1])*float(TermDict[i[0]]))
                            if(opp1):
                                opp1 = False
                            elif(opp2):
                                opp2 = False

                    if(TermIDDict.has_key(i[0])):
                        if(SVM.has_key(int(TermIDDict[i[0]]))):
                            SVM[int(TermIDDict[i[0]])] += int(i[1])
                        else:
                            SVM[int(TermIDDict[i[0]])] = int(i[1])
                    else:
                        TermIDDict[i[0]] = 1
                        if(SVM.has_key(int(TermIDDict[i[0]]) )):
                            SVM[int(TermIDDict[i[0]])] += int(i[1])
                        else:
                            SVM[int(TermIDDict[i[0]])] = int(i[1])


                    countWords += 1

                    if((countWords > 100)):

                        SortedDocDict = []
                        DocDict.clear()
                        break

                sorted_SVM = sorted(SVM.items(), key=operator.itemgetter(0), reverse = False)

                for j in sorted_SVM:
                    f2.write(str(j[0])+":"+str(j[1])+" ")



                f2.write("\t#"+ str(D[2]) + "\n")
                score /=3.0
                RealWords /= 3.0
                HardWords /= 3.0
                SVM.clear()

                if(average.has_key(D[2])):
                    average[D[2]] += score
                else:
                    average[D[2]] = score

                if(std.has_key(D[2])):
                    std[D[2]].append(score)
                else:
                    std[D[2]] = [score]

                fo.write("\nNo:" + str(count) + "\n")
                fo.write("Total Words Count:"+str(len(content.split()))+"\n")
                fo.write("Total Words Count without Stop words:"+str(len(CleanTerm))+"\n")
                fo.write("Each write:"+str(RealWords)+"\n")

                fo.write("Original word count:" + str(len(lent)) + "\n")
                fo.write("Hard Words:"+str(HardWords)+"\n")
                fo.write("Percentage of Hard Words:"+str(float(HardWords/(RealWords*1.0)))+"\n")
                # fo.write("Penalty Words:"+str(FinalWords)+"\n")
                fo.write("Real Score:"+str(score)+"\n")
                # fo.write("Penalty Score:"+str(FinalScore)+"\n")
                fo.write("Reading Level :"+str(D[2])+"\n\n")
                # fo.write("Predict Level:"+str(float(score/(RealWords)))+"\n\n\n")
                # fo.write("Penalty Level:"+str(float(score/(FinalWords)))+"\n\n\n\n")


                print("\nNo:" + str(count))
                print("Total Words Count:"+str(len(content.split())))
                print("Total Words Count without Stop words:"+str(len(CleanTerm)))
                print("Each write:"+str(RealWords))
                print("Hard Words:"+str(HardWords))
                print("Percentage of Hard Words:"+str(float(HardWords/(RealWords*1.0))))
                # print("Penalty Words:"+str(FinalWords))
                print("Score:"+str(score))
                # print("Penalty Score:"+str(FinalScore))
                print("Reading Level :"+str(D[2]))
                # print("Level:"+str(float(score/(RealWords))))
                # print("Penalty Level:"+str(float(score/(FinalWords)))+"\n")



x = []
y = []
err = []
for i in sorted(std):
    for j in std[i]:
        plt.scatter(str(i), (j), c="red", alpha=0.4)
    ave = average[str(i)]/(len(std[str(i)]))
    e = np.std(sorted(std[str(i)]))
    x.append(str(i))
    y.append(ave)
    err.append(e)
    # if(str(i) == "ADULT"):
    #     ax.errorbar(6, ave, yerr=e, c="blue", alpha=0.5,linestyle='None', marker='^')
    #
    # elif(str(i) == "ALL"):
    #     ax.errorbar(7, ave, yerr=e, c="blue", alpha=0.5,linestyle='None', marker='^')
    # else:

    fo.write("Level " + str(i) +": average - "+str(ave)+", std - " + str(e)+"\n")
    print("Level " + str(i) +": average - "+str(ave)+", std - " + str(e))


df = pd.DataFrame({"x":x,
                   "mean":y,
                   "sd":err})
plt.errorbar(range(len(df['x'])), df['mean'], yerr=df['sd'], fmt='^')
plt.xticks(range(len(df['x'])), df['x'])
plt.plot(df['x'], df['mean'], '.-', color = 'y')
# for i in std:


# plt.errorbar(np.arange(len(df['x'])), df['mean'], yerr=df['sd'], ls='None', marker='o')
# for i in range(len(x)):
#     ax.errorbar(x[i], y[i], yerr=err[i], c="blue", alpha=0.5,linestyle='None', marker='^')

# fo.write("Level 0: "+str(average["0"])+"\n")
# fo.write("Level 1: "+str(average["1"])+"\n")
# fo.write("Level 2: "+str(average["2"])+"\n")
# fo.write("Level 3: "+str(average["3"])+"\n")
# fo.write("Level 4: "+str(average["4"])+"\n")
# fo.write("Level 5: "+str(average["5"])+"\n")
# fo.write("Level ALL: "+str(average["ALL"])+"\n")
# fo.write("Level ADULT: "+str(average["ADULT"])+"\n")

ax.legend()
ax.grid(True)
plt.savefig('SVM 100 Features FIX FINAL Test.pdf')
plt.show()
t = datetime.datetime.now()
fo.write(str(t))
fo.close()
f2.close()
# The stop word count as part of 500 words, but it did not count the score
