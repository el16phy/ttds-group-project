from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from nltk import pos_tag, word_tokenize
import string
"""
Created on 4, Jan, 2019

@author: Po-Hung Yeh
@version: 1.0
@Purpose: Lemmatized the string value
"""

class Lemmatizer:
    def __init__(self):
        # Convert all the tags to wordnet form
        self.pos_to_wornet_dict = {

            'JJ': wordnet.ADJ,
            'JJR': wordnet.ADJ,
            'JJS': wordnet.ADJ,
            'RB': wordnet.ADV,
            'RBR': wordnet.ADV,
            'RBS': wordnet.ADV,
            'NN': wordnet.NOUN,
            'NNP': wordnet.NOUN,
            'NNS': wordnet.NOUN,
            'NNPS': wordnet.NOUN,
            'VB': wordnet.VERB,
            'VBG': wordnet.VERB,
            'VBD': wordnet.VERB,
            'VBN': wordnet.VERB,
            'VBP': wordnet.VERB,
            'VBZ': wordnet.VERB,

        }

        self.lemmatizer = WordNetLemmatizer() # init the Lemmatizer

    def RunLemmatizer(self, sentence):
        '''
        @Last Edited on 5, Jan, 2019
        @Last Editor : Po-Hung Yeh
        @purpose:
            The function will receive a input string and reuturn the lemmatized string

        @arg:
            (string) sentence: the input string value
        @return:
            (string) str1: the output string value after lemmatizing

        '''

        word = ""
        ReturnWord = [] # stores return words
        tagged = pos_tag(word_tokenize(string.lower(sentence))) # tag the word with different form(verb, noun, adj, adv)

        for i in tagged:
            if(self.pos_to_wornet_dict.has_key(i[1])):
                word = (self.lemmatizer.lemmatize(i[0], pos= (self.pos_to_wornet_dict[i[1]]))) #the word has been Lemmatize
            else:
                word = i[0] #original word without changing
            ReturnWord.append(word)
        str1 = ' '.join(ReturnWord) # return the words as string
        return str1.encode("utf-8")

L = Lemmatizer()
# fo = open("/afs/inf.ed.ac.uk/user/s18/s1852861/Desktop/INFR11145/Group_Project/ttds-group-project/Word Frequency/Sample 2/S22.txt", "r") # write the feats dic
# c = fo.read()
# fo.close()

# fo = open("L22.txt", "w")
ret = L.RunLemmatizer("he knew that")
print(ret)
# fo.write(ret)
# fo.close()

#---------------------------------
# Sample and Q&A

# https://pythonprogramming.net/lemmatizing-nltk-tutorial/
# http://www.nltk.org/_modules/nltk/corpus/reader/wordnet.html
# https://stackoverflow.com/questions/15586721/wordnet-lemmatization-and-pos-tagging-in-python
# https://stackoverflow.com/questions/27591621/nltk-convert-tokenized-sentence-to-synset-format
# https://www.nltk.org/api/nltk.stem.html
# https://stackoverflow.com/questions/25534214/nltk-wordnet-lemmatizer-shouldnt-it-lemmatize-all-inflections-of-a-word/25544239

from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()

print(lemmatizer.lemmatize("he knew that"))
# print(lemmatizer.lemmatize("cats"))
# print(lemmatizer.lemmatize("cacti"))
# print(lemmatizer.lemmatize("geese"))
# print(lemmatizer.lemmatize("rocks"))
# print(lemmatizer.lemmatize("python"))
# print(lemmatizer.lemmatize("better"))
# print(lemmatizer.lemmatize("better", pos="a"))
# print(lemmatizer.lemmatize("best", pos="a"))
# print(lemmatizer.lemmatize("ran"))
# print(lemmatizer.lemmatize("interested", pos = "v"))
# print(lemmatizer.lemmatize("interesting", pos = "a"))
# print(lemmatizer.lemmatize("ran",'v'))
# print(lemmatizer.lemmatize("Ran",'v'))

# print(lemmatizer.lemmatize("happier",pos='a'))
# print(lemmatizer.lemmatize("happily", pos = wordnet.ADV))
# print(lemmatizer.lemmatize("likely",pos='a'))

# print(wordnet.ADV)
