import os
import math
import re
from nltk.stem.porter import PorterStemmer
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt


##This function is used to tokenise. The input is a string. Then "'" is replaced by ""(blank).
##All the other symbols are replaced by " "(space). Then the string is split by space into a list
def tokenize(stringTo):
    a=stringTo.replace("-"," ")
    b=a.replace("'","")
    c=re.sub(r'[^\w\s]'," ",b)

    return c.split()

##Read stop words and stored in a list
stopwords=[]
def readStopWords():
    fi=open("stopwords.txt", "r+")
    rFile=fi.readlines();
    for line in rFile:
        a=re.sub(r'[^\w\s]',"",line)
        stopwords.append(a.strip("\n"))
    fi.close()

##read wiki and store all words in dictionary.
dicNum={}  ##the key is word, the value is the number of this word, eg "the"=1
dicFre={}  ##the key is word, the value is the frequency of this word, eg "the"=1940528
def readWiki():
    fi=open("TermFrequency2.txt", "r+")
    rFile=fi.readlines();
    count=0
    for line in rFile:
        count=count+1
        words=line.split()
        dicNum[words[0]]=count
        dicFre[words[0]]=words[1]

    fi.close()

## read train data and test data .xml
def readData(fileName):
    tree = ET.parse(fileName)
    root = tree.getroot()

    docNum=[]
    level=[]
    contents=[]
    for child in root:
        for item in child:
            item_att=item.tag
            if item_att=='DOCNO':
                docNum.append(item.text)
            item_att=item.tag
            if item_att=='LEVEL':
                level.append(item.text)
            item_att=item.tag
            if item_att=='CONTENTS':
                contents.append(item.text)

    for ID,lev,con in zip(docNum,level,contents):
        wordId=[] ## the id of word according to wiki
        wordFre=[] ## the frequency of word according to wiki
        wordFreDic={}


        tempcount=0


        allWords2Values=[] ##store all words' values
       # f=open("ID_"+str(ID)+"_LEV_"+str(lev)+".txt", "w")
        afTokens=tokenize(con)
        for each in afTokens:
            afLower=each.lower()
            if afLower not in stopwords:
                if afLower in dicNum.keys():
                    word2id=dicNum[afLower]
                    allWords2Values.append(word2id)
                    if word2id>55000:
                        tempcount=tempcount+1
                    if word2id in wordFreDic:
                        k=wordFreDic[word2id]
                        k=k+1
                        wordFreDic[word2id]=k
                    else:
                        wordFreDic[word2id]=1
        if len(allWords2Values)>10000:
            print("ID_"+str(ID)+"_LEV_"+str(lev)+"       "+str(len(allWords2Values))+"          "+str(100*tempcount/len(allWords2Values)))

# =============================================================================
#         wordId=wordFreDic.keys()
#         wordFre=wordFreDic.values()
#         d=list(zip(wordId,wordFre))
#         d.sort(reverse=False)
#         wordId,wordFre= zip(*d)
#         for II,FF in zip(wordId,wordFre):
#             f.write(str(II)+"  "+str(FF)+"\n")
#         f.close()
# =============================================================================
#
# # id-fre scatter plot and saving
# =============================================================================
#         plt.scatter(wordId,wordFre,s=1)
#         plt.xlim(0, 285348)
#         plt.xlabel("word")
#         plt.ylabel("frequency")
#         plt.savefig("SCATTER_ID_"+str(ID)+"_LEV_"+str(lev)+".pdf")
#         plt.show()
# =============================================================================
#
# # id-fre histogram plot and saving
# =============================================================================
        plt.xlim(0, 285348)
        #plt.yticks([-7,-6,-5,-4])
        plt.ylim(top=(0.0001))  # adjust the top leaving bottom unchanged
        plt.ylim(bottom=(0.0000001))  # adjust the bottom leaving top unchanged
        plt.yscale('log')
        plt.xlabel("word")
        plt.ylabel("frequency")
        plt.hist(allWords2Values, 30, normed=True)
        plt.savefig("hist_ID_"+str(ID)+"_LEV_"+str(lev)+".pdf")
        plt.show()
# =============================================================================






readStopWords()
readWiki()
readData("data_1.xml")

# =============================================================================
# readWiki()
# print(len(dicNum))
#
# plt.scatter(dicNum.values(),dicNum.values())
# =============================================================================


class preprocessing(object):

    def __init__(self,name):
        self.filename=name                    ##Source .xml file
        self.dic={}                           ##Dictionary for storing information
        self.stemmer=PorterStemmer()

##Read .xml file and process it (tokenisation,case folding,stopping and normalization)
##Create a dictionary storing all the information
    def process(self):
        tree = ET.parse(self.filename)
        root = tree.getroot()

        for child in root:
            sentence=""
            docId=""
            for item in child:
                item_att=item.tag
                if item_att=='HEADLINE':
                    sentence=item.text
                item_att=item.tag
                if item_att=='DOCNO':
                    docId=item.text
                item_att=item.tag
                if item_att=='TEXT':
                    sentence=sentence+item.text

            afTokens=tokenize(sentence)
            count=0
            for each in afTokens:
                afLower=each.lower()
                if afLower not in stopwords:
                    term=self.stemmer.stem(afLower)
                    count=count+1
                    if term in self.dic:
                        allList=self.dic[term]
                        if docId not in allList[0]:
                            allList[0].append(docId)
                            allList.append([count])
                        else:
                            pos=allList[0].index(docId)
                            allList[pos+1].append(count)
                    else:
                        self.dic[term]=[[docId],[count]]

##Output positional inverted index file
    def writeIndex(self):
        self.process()
        newName="index.txt"
        fw = open(newName,"w")

        Lkeys=[]
        for each in self.dic:
            Lkeys.append(each)
        Lkeys.sort()
        for term in Lkeys:
            allList=self.dic[term]
            docnum=len(allList[0])
            fw.write(term+":("+str(docnum)+")")
            fw.write("\n")
            for ii in range(docnum):
                docIdList=allList[0]
                fw.write("   "+str(docIdList[ii])+":")
                tf=len(allList[ii+1])
                fw.write("("+str(tf)+")")
                for jj in allList[ii+1]:
                    fw.write("  "+str(jj))
                fw.write("\n")
            fw.write("\n")
        fw.close()
