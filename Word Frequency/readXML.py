# -*- coding: utf-8 -*-
"""
Created on 4, Jan, 2019

@author: Po-Hung Yeh
@version: 1.0.1
@Purpose: Read the XML file
"""

import xml.etree.ElementTree as ET
import string
import re

class readXML:
    def containsNonAscii(self, s):
        #https://stackoverflow.com/questions/41126053/removing-words-that-contain-non-ascii-characters-using-python
        return any(ord(i)>127 for i in s)

    def CheckEnglish(self, content):
        '''
        @Last Edited on 4, Jan, 2019
        @Last Editor : Po-Hung Yeh
        @purpose:
            The function will check if the content is english words/terms or not

        @arg:
            (string) content: a string content
        @return:
            (string) result.strip(): the output string value with english characters/words only

        '''
        # Remove all the words contain non-English characters
        cleaned_words = [word for word in content if  not self.containsNonAscii(word)]
        cleaned_sentence = ''.join(cleaned_words)
        # print(cleaned_sentence)
        result = (re.sub(r'[^\w\s]',' ',cleaned_sentence)).strip() # remove all the punctuations
        result = (re.sub('\d+',' ',result)).strip() # remove all digit

        return result.strip()
    # .decode('utf-8')

    def Run(self, FileName):
        '''
        @Last Edited on 4, Jan, 2019
        @Last Editor : Po-Hung Yeh
        @purpose:
            The function will receive a input string and reuturn the lemmatized string

        @arg:
            (string) FileName: Path or Name of the xml file
        @return:
            (string) str1: the ouput list wiki content
                1. DocID
                2. Wiki Title
                3. Wiki Content
                [[[DocID],[Title],[Content]], [DocID],[Title],[Content]...]
        '''
        tree = ET.parse(FileName) # read XML
        root = tree.getroot() # seperate all the attribute
        DocList = [] # Doc list has all the content from give wiki
        WikiList = [] # wiki list has all the Doc list content
        ContentList = []
        for child in root:
            for item in child:
                item_att = item.tag # take the attribute
                if(item_att == "DOCNO"): # Document ID
                    DocID = (item.text).strip()
                    DocList.append(DocID)
                if(item_att == "Title"): # Title of wiki
                    DocTitle = (item.text).strip()
                    DocList.append(DocTitle)
                if(item_att == "Text"): # Content of wiki
                    DocText = (item.text).strip()
                    DocText = self.CheckEnglish(DocText.split()).encode("utf-8")
                    DocList.append(DocText) # store in Wiki list
                    # ContentList.append(DocText)
            WikiList.append(DocList)
            DocList = [] # clear the Doc lit

        # fo = open("S32.txt", "w") # write the feats dic
        # str1 = ' '.join(ContentList).encode("utf-8")
        # fo.write(str1)
        # fo.close()
        return WikiList
