from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
import operator
import math
import numpy as np
'''
@Last Edited on 4, Jan, 2019
@Last Editor : Po-Hung Yeh
@purpose:
    Generate the Diagram of term frequency

'''

# ValueList = [] # store the frequency value
# stopWords = set(stopwords.words('english'))
# with open("TermFrequency2.txt") as f:
#     content = f.readlines()
# f.close()
#
# # fo = open("TermFreuqency_WithStopWords S4 8w5 0_00007.txt", "w")
# count = 0
# for i in content:
#     key, value = i.split("\t")
#     if str.lower(str(key)) not in stopWords:
#         count += 1
#         # fre = 1 / (0.25*(1 + np.exp(-0.00007*(count-85000))))
#         # if(count >=50000):
#         #     fre = 1 / (1 + np.exp(-0.00003*(count-50000)))
#         # else:
#         #     fre = 0
#         # ValueList.append(fre)
#         # fo.write(str(key)+"\t"+str(value.strip()) + "\t" + str(fre) +"\n")
#         ValueList.append((float(value.strip())))
# # fo.close()
# # ValueList = [1,2,3,4,5,6]
# print(len(ValueList))
# # print(ValueList[:20])
# plt.plot(ValueList[:250000])
# # plt.plot(np.log(np.asarray(ValueList[:250000])))
#
# # plt.gca().invert_yaxis()
# # plt.xticks([])
# # plt.yticks([])
# plt.ylabel('Frequency')
# plt.savefig('TermFreuqency_WithStopWords.pdf')
#
# # plt.savefig('TermFreuqency_WithStopWords S4 8w5 0_00007.png')
# plt.show()





import matplotlib.pylab as plt
import numpy as np
import math
x = np.arange(0, 250000)
# f = 1 / (1 + np.exp(-(1 / (1 + np.exp(-(1 / (1 + np.exp(-(1 / (1 + np.exp(-(1 / (1 + np.exp(-(1 / (1 + np.exp(-(1 / (1 + np.exp(-(1 / (1 + np.exp(-(1 / (1 + np.exp(-(1 / (1 + np.exp(-(1.0 / (1 + np.exp(-(x-50000)))))))))))))))))))))))))))))))))
# f = np.log(f)
# x = np.arange(-8, 8, 0.1)
# f = (1 / pow((1 + np.exp(-(x))), 10))
fre = 1 / (0.5*(1 + np.exp(-0.00007*(x-85000))))
# f = np.log(f)
# f = np.log(f)
plt.plot(x, fre)
plt.xlabel('x')
plt.ylabel('f(x)')
plt.savefig('TermFreuqency_WithStopWords S4 8w5 0_00007.pdf')

plt.show()
