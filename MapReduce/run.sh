# OUTPUT_DIR=/user/${USER}/assignment/task3
# OUTPUT_FILE=output.out
#
# # Hadoop won't start if the output directory already exists
# hdfs dfs -rm -r $OUTPUT_DIR
#
# hadoop jar /opt/hadoop/hadoop-2.9.1/share/hadoop/tools/lib/hadoop-streaming-2.9.1.jar \
#   -D mapreduce.job.name=${USER}_MapReduce \
#   -D stream.num.map.output.key.fields=1 \
#   -D mapred.reduce.tasks=1 \
#   -input /afs/inf.ed.ac.uk/user/s18/s1852861/Desktop/INFR11145/Group_Project/ttds-group-project/Developing/MapReduce/L1.txt \
#   -input /afs/inf.ed.ac.uk/user/s18/s1852861/Desktop/INFR11145/Group_Project/ttds-group-project/Developing/MapReduce/L2.txt \
#   -output $OUTPUT_DIR \
#   -mapper Mapper1.py \
#   -combiner Combiner1.py \
#   -reducer Reducer1.py \
#   -mapper Mapper.py \
#   -reducer Reducer.py \
#   -file Mapper1.py \
#   -file Combiner1.py \
#   -file Reducer1.py \
#   -file Mapper2.py \
#   -file Reducer2.py

# hdfs dfs -cat ${OUTPUT_DIR}/part-* > $OUTPUT_FILE
# cat $OUTPUT_FILE


cat  L12.txt L22.txt L32.txt L42.txt |./Mapper1.py | sort -k1,1 | ./Reducer1.py | sort -nrk1,1 | ./Reducer2.py
