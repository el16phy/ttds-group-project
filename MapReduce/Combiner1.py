#!/usr/bin/env python2
#combiner
import sys


CurrentWords  = ""
Count = 1
Check = True

for line in sys.stdin:
    # Parse key and value
    key, value = line.strip().split('\t') # key and value

    if(Check):
        CurrentWords = key
        Count = int(value)
        Check = False
    elif(CurrentWords == key):
        Count += 1
    else:
        print(str(CurrentWords) + "\t" + str(Count))
        CurrentWords = key
        Count = int(value)


print(str(CurrentWords) + "\t" + str(Count)) # print out the last one
