# -*- coding: utf-8 -*-

"""
Created on Fri Dec 21 16:45:37 2018

@author: Lydia & Po-Hung Yeh
@version: 2.0
"""


import wikipedia


def WriteFile(obj, count, title, content):
    '''
    This function is used to write the xml file

    arg:
    obj: obj is the obj of write file document
    count: count how many wiki content has been found
    title: title of the content
    content: content of wiki
    '''
    obj.write('<DOC>\n')

    obj.write('<DOCNO>'+str(count)+'</DOCNO>'+'\n') # write count

    obj.write('<Title>'+title+'</Title>'+'\n') # write title

    obj.write('<Text>'+'\n')
    obj.write(content)
    obj.write('\n'+'</Text>'+'\n')
    obj.write('</DOC>'+'\n\n')


urls_file = open("titles.txt", encoding='utf-8') # read entries file
urls = urls_file.readlines()
urls_file.close()

SimpleFile = open('Simple_Wiki.xml', 'a+', encoding='utf-8') # write simple
RegularFile = open('Regular_Wiki.xml', 'a+', encoding='utf-8') # write regular
LostFile = open('lost.txt', 'a+', encoding='utf-8') # write lost and Multiple

count = 1 # count how many total wiki has been found
# starting from 1
TotalRun = 0
TotalMultiple = 0
TotalPageNotFound = 0

for i in urls[50:55]:
    # NewContent = i.strip()
    TotalRun += 1 # starting from 1
    NewContent = i.strip().replace("https://simple.wikipedia.org/wiki/", "")
    # print(NewContent)
    check = True # the default check is true
    print(count)
    try:
        wikipedia.set_lang("en") # set the language to regular english
        regular = wikipedia.summary(NewContent, auto_suggest= False) # find item in regular
        # print(regular)
        WriteFile(RegularFile, count, NewContent, regular) # write regular file
        count += 1 # add one if item is found

    except wikipedia.exceptions.DisambiguationError: #if the items has Multiple content
        print(NewContent)
        print("Multiple_Pages_Found\n")
        check = False # if the content is not found, trun off check
        TotalMultiple += 1
        LostFile.write(NewContent + "\t"+ "Multiple\n")
    except wikipedia.exceptions.PageError: # if the item is not found
        print(NewContent) # print out the not found item
        print("Page Not found\n")
        check = False # if the content is not found, trun off check
        TotalPageNotFound += 1
        LostFile.write(NewContent + "\t"+ "Not_Found\n")

    if(check):
        wikipedia.set_lang("simple") # set the language to simple language
        simple = wikipedia.summary(NewContent, auto_suggest = False) # find item in simple
        # print(simple)
        WriteFile(SimpleFile, count, NewContent, simple) # write simple fil

    else:
        print("Skip\n")
        
LostFile.write("\nTotal Run:" + str(TotalRun) + "\n")
LostFile.write("\nTotal Successful:" + str(TotalRun - TotalMultiple - TotalPageNotFound) + "\n")
LostFile.write("\nTotal Multiple:" + str(TotalMultiple) + "\n")
LostFile.write("Total not found:" + str(TotalPageNotFound) + "\n")
# close the file
SimpleFile.close()
RegularFile.close()
LostFile.close()
