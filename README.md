# TTDS Group Project

Created on 1, Jan, 2019
Last Update: 4, Jan, 2019
Editor: Po-Hung Yeh


Git global setup
git config --global user.name "USER NAME"
git config --global user.email "EMAIL ADDRESS"


Existing folder
cd existing_folder
git init
git remote add origin git@gitlab.com:el16phy/ttds.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:el16phy/ttds.git
git push -u origin --all
git push -u origin --tags

-------------------------------------------
Folder Info
CatchWiki Folder contains all the contains extract from the Regular Wiki and Simple Wiki
Developing Folder contains all on-going file
MapReduce Folder contains all the MapReduce python file for calculating the term frequency
Terms Folder contains all the terms after processing
Word Frequency Folder contains the Lemmatizer operating, ReadXML operating, and Generating the Diagram operating
