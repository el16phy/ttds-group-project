from nltk.stem.porter import PorterStemmer
import xml.etree.ElementTree as ET
import requests
from bs4 import BeautifulSoup

def getChaptersUrl(bookUrl,contentUrl): #get chapters URLs of a book
    #bookUrl is the full url of a book, "http://www.gatewaytotheclassics.com/browse/display.php?author=comstock&book=trees&story=_contents"
    #contentUrl is the part url of a book, "http://www.gatewaytotheclassics.com/browse/"   
    urlList=[]
    res = requests.get (bookUrl)
    soup = BeautifulSoup(res.content, "html5lib")
    tables=soup.findAll('table') 
    tab = tables[2] 
    
    for d in tab.findAll('div'):       
        for a in d.findAll('a'):      
            partChapterUrl=a.get("href")
            URL=contentUrl+partChapterUrl
            urlList.append(URL)
    urlList.pop(0)
    
    return urlList


def getChaptersContents(urlList): #get chapter contents of a book
    contents=""
    for url in urlList:
        res = requests.get (url)
        soup = BeautifulSoup(res.content, "html5lib")
        tables=soup.findAll('table') 
        #print(url)
        tab = tables[2] 
        for p in tab.findAll('p'):
            content=p.getText()
            contents=contents+" "+content
            
    return contents
           

def getBookUrl(url): #get URLs of all books
    f=open("data.xml", "w",encoding='utf-8')
    res = requests.get (url)
    soup = BeautifulSoup(res.content, "html5lib")
    tables=soup.findAll('table') 
    tab = tables[1]  

    countBooks=0 #Count the number of books
    partUrl=""
    for tr in tab.findAll('tr'):
        countBooks=countBooks+1
        print(countBooks)
        temp=[] #Storing the contents of each td
        for td in tr.findAll('td'):
            br=td.getText() 
            if br!="\xa0":
                temp.append(br.replace('\xa0', ""))  
            
            for a in tr.findAll('a'):
                partUrl=a.get("href") #part url of each book
                
        bookURL="http://www.gatewaytotheclassics.com/browse/" #complete url of each book

        if countBooks>515: #The title of the table is empty
            f.write("<DOC>"+"\n")
            f.write("<DOCNO>"+str(countBooks)+"</DOCNO>"+"\n")
            f.write("<GENRE>"+temp[-1]+"</GENRE>"+"\n")
            f.write("<LEVEL>"+temp[-2]+"</LEVEL>"+"\n")
            authorAndName=temp[-3]
            bookName=authorAndName.split(", ")
            f.write("<BOOKNAME>"+bookName[1]+"</BOOKNAME>"+"\n")
            
            chapterUrl=getChaptersUrl(bookURL+partUrl,bookURL)
            contents=getChaptersContents(chapterUrl)
            f.write("<CONTENTS>"+contents+"\n"+"</CONTENTS>"+"\n"+"</DOC>"+"\n")
            
            
            
       
    f.close()
    print("finished")


    

url="http://www.gatewaytotheclassics.com/browse/books_browse_by_level.php"
#getChaptersUrl("http://www.gatewaytotheclassics.com/browse/display.php?author=steedman&book=bible&story=_contents","http://www.gatewaytotheclassics.com/browse/")
getBookUrl("http://www.gatewaytotheclassics.com/browse/books_browse_by_level.php")

